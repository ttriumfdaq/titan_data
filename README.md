# Overview

This package contains python tools for reading TITAN PPG midas files.

It includes:

* core tools for parsing midas files (copied from the main midas repository, to make TITAN analyzer life easier)
* EBIT/CPET/MPET-specific tools for understanding the data in the midas files from each experiment
* command-line tools for dumping data to screen, either from a file or for a live experiment
* helper functions for the python-based online analyzers for each experiment (accessed through the "Web plots" webpages)

# Installation

## Clone

First, clone this repository with:

```
git clone https://bitbucket.org/ttriumfdaq/titan_data.git
```

## Install

Then, the simplest way to "install" this package is to just edit your `PYTHONPATH` environment variable:

```
export PYTHONPATH=$PYTHONPATH:/path/to/titan_data
```

Alternatively you can install the package using [pip](https://pypi.org/project/pip/) (the `-e` flag means you won't have to re-install each time you pull an update from the git repository).

```
cd /path/to/titan_data
pip install -e .
```

# Usage

Example usage is:

```
import titan_data.ebit

ebit_file = titan_data.ebit.EBITFile("/path/to/midas_file.mid")

for ebit_event in ebit_file:
    print("FC3 reading was %s" % ebit_event.faraday_cup_readings["ILE2T:FC3:SCALECUR"])
```

The code supports both python2.7 and python3.

## Python objects

The objects you work with are defined in `titan_data/ebit/__init__.py`, `titan_data/mpet/__init__.py` and `titan_data/cpet/__init__.py`.

All 3 experiments follow a similar structure:

* `titan_data.ebit.EBITFile` is used to open a midas file and iterate over the events in it
* `titan_data.ebit.EBITRunInfo` contains information about the run as a whole (e.g. run number, start time, what variables were being scanned)
* `titan_data.ebit.EBITEvent` contains information about a single midas event (i.e. the data you actually care about)

See the documentation in the relevant `__init__.py` file for the contents of each object.


