"""
Tools for parsing CPET data.


Example usage for iterating over events in python:

```
import titan_data.cpet

cpet_file = titan_data.cpet.CPETFile("/path/to/midas_file.mid")

for cpet_event in cpet_file:
    print(cpet_event.event_time)
```



Example usage for grabbing all events as a list:

```
import titan_data.cpet

cpet_file = titan_data.cpet.CPETFile("/path/to/midas_file.mid")
cpet_events = cpet_file.get_all_event_info()

for cpet_event in cpet_events:
    print(cpet_event.event_time)
```
"""

import titan_data.midas as midas
import titan_data.midas.file_reader as file_reader
import datetime

class CPETSummary:
    """
    Class that parses a full CPET midas file and summarizes the data:
    * Sums the MCA4 data
    
    Separate sums/lists are created for the different scan configurations
    if scanning is enabled (e.g. the MCA data with a PPG offset of 2ms is
    summed separately from the data with an offset of 5ms).
    
    If you don't like the summary info presented, you can create an CPETFile
    object instead and iterate over the events manually.
    
    Members:
    
    * mca_summary (list of dicts) - Dict contents are:
        > x (int) - X step of scan
        > y (int) - Y step of scan
        > ppg (dict of {str: number}) - Value of any PPG variables being scanned
        > ppg_muted (dict of {str: boolean}) - Whether any PPG blocks were being automatically muted/unmuted
        > epics (dict of {str: number}) - Value of any EPICS variables being scanned
        > chan_1 {list of int} - MCA4 channel 1 histogram (summed from whole run)
        > chan_2 {list of int} - MCA4 channel 2 histogram (summed from whole run)
        > sr430 {list of int} - SR430 histogram (summed from whole run)
    * run_info (CPETRunInfo) - Run-level information (e.g. start time, MCA bin width, etc)
    * run_comment (str) - Comments provided by user during run
    """
    def __init__(self, file_path):
        cpetfile = CPETFile(file_path)
        self.mca_summary = []
        
        mca4_totals_dict = {}
        
        for ev in cpetfile:
            if ev is None:
                continue
            
            if ev.x_step not in mca4_totals_dict:
                mca4_totals_dict[ev.x_step] = {}
                
            if ev.y_step not in mca4_totals_dict[ev.x_step]:
                mca4_totals_dict[ev.x_step][ev.y_step] = {"x": ev.x_step,
                                                          "y": ev.y_step,
                                                          "ppg": ev.ppg_scan_values,
                                                          "ppg_muted": ev.ppg_muted,
                                                          "epics": ev.epics_scan_values,
                                                          "chan_1": list(ev.mca4_chan1),
                                                          "chan_2": list(ev.mca4_chan2),
                                                          "sr430": list(ev.sr430)}
            else:
                for i, v in enumerate(ev.mca4_chan1):
                    mca4_totals_dict[ev.x_step][ev.y_step]["chan_1"][i] += v
                    
                for i, v in enumerate(ev.mca4_chan2):
                    mca4_totals_dict[ev.x_step][ev.y_step]["chan_2"][i] += v
                    
                for i, v in enumerate(ev.sr430):
                    mca4_totals_dict[ev.x_step][ev.y_step]["sr430"][i] += v
        
        for x in mca4_totals_dict.values():
            for f in x.values():
                self.mca_summary.append(f)
        
        self.run_info = cpetfile.run_info

    def dump_to_screen(self):
        print("")
        print("Run info:")
        print("=" * 78)
        
        print("Run number            : %s" % self.run_info.run_number)
        print("Run start time        : %s (%s)" % (self.run_info.run_start_time, datetime.datetime.fromtimestamp(self.run_info.run_start_time)))
        print("MCA bin width (ns)    : %s" % self.run_info.mca4_bin_width_ns)
        print("SR430 bin width (ns)  : %s" % self.run_info.sr430_bin_width_ns)
        
        print("")
        print("Data summary:")
        print("=" * 78)
        
        for data in self.mca_summary:
            print("X point %d, Y point %d" % (data["x"], data["y"]))
            
            if len(data["ppg"]):
                print("  PPG scanning:")
                for k, v in data["ppg"].items():
                    print("    %s => %s" % (k, v))
            if len(data["epics"]):
                print("  EPICS scanning:")
                for k, v in data["epics"].items():
                    print("    %s => %s" % (k, v))
            
            for mca_chan in ["1", "2"]:        
                max_val = 0
                max_bin = None
                    
                for i, v in enumerate(data["chan_%s" % mca_chan]):
                    if v > max_val:
                        max_bin = i
                        max_val = v
            
                print("  MCA4 channel %s has %s bins. Max bin is %s with %s counts" % (mca_chan, len(data["chan_%s" % mca_chan]), max_bin, max_val))
     
            max_val = 0
            max_bin = None
            
            for i, v in enumerate(data["sr430"]):
                if v > max_val:
                    max_bin = i
                    max_val = v
        
            print("  SR430 has %s bins. Max bin is %s with %s counts" % (len(data["sr430"]), max_bin, max_val))
 
class CPETFile:
    """
    Class that provides easy access to CPET-specific data .
    """
    def __init__(self, file_path):
        self.midas_file = file_reader.MidasFile(file_path)
        self.run_info = get_cpet_run_info(self.midas_file.get_bor_odb_dump().data)
        
    def get_all_event_info(self):
        """
        Returns data for all events in a file as a list.
        This may be slow and memory-inefficient if you have a large number
        of events in the file.
        """
        return [ev for ev in self]

    def __next__(self):
        """
        Iterable interface for looping through events.
        """
        while True:
            midas_event = self.midas_file.read_next_event()
            
            if midas_event is None:
                # End of file - stop iterating
                raise StopIteration()
            
            # Try to convert to an CPETEvent
            cpet_event = get_cpet_event_info(midas_event, self.run_info)
            
            return cpet_event
    
    next = __next__ # for Python 2        
        
    def __iter__(self):
        """
        Iterable interface for looping through events.
        """
        return self

class CPETRunInfo:
    def __init__(self):
        """
        Overall configuration of voltage/EPICS scanning etc.
        
        If scanning is disabled, most of these lists will be empty.
        
        Members:
            
        * run_number (int) - Midas run number
        * run_start_time (int) - UNIX timestamp when run started
        * num_x_steps (int) - Number of X steps in scan (data is taken at N+1 points)
        * num_y_steps (int) - Number of Y steps in scan (data is taken at N+1 points)
        * scan_ppg_x (list of str) - list of ODB paths changed in each X step
        * scan_ppg_y (list of str) - list of ODB paths changed in each Y step
        * scan_epics_x (list of str) - list of devices changed in each X step
        * scan_epics_y (list of str) - list of devices changed in each Y step
        * scan_lvv_x (list of str) - list of LabView Voltage parameters changed in each X step
        * scan_lvv_y (list of str) - list of LabView Voltage parameters changed in each Y step
        * scan_dac_x (list of str) - list of CPET DAC ODB paths changed in each X step
        * scan_dac_y (list of str) - list of CPET DAC ODB paths changed in each Y step
        * dac_defaults (list of float) - CPET DAC voltages if DAQ is controlling DAC, but not scanning
        * flip_bank_order (list of string) - Order of "flippable" devices reported in the FLIP bank
        * flip_state_meanings (dict of {str: {True: str, False: str}} - Human readable meanings of True/False 
            as reported in FLIP bank for each flippable device
        * ppg_mute_polarity (dict of {str: bool}) - Block name to whether "mute then unmute" or opposite
        * mca4_bin_width_ns (int) - How wide each bin in the MCA4 histograms are, in ns
        * sr430_bin_width_ns (int) - How wide each bin in the SR430 histogram is, in ns
        * constant_scope_background_uVsecs (float) - Constant background to subtract from MCP signal to 
            calculate number of electron. If None, no subtraction will happen, and the user is responsible
            for doing it (e.g. by comparing events with the "ES Wehnelt" pulse muted and unmuted).
        * electron_area_factor (float) - Conversion from area in uV*seconds to number of electrons
        * scope_width_secs (float) - Duration over which the electron area was integrated on the scope.
        * ppg_program (dict) - PPG program being executed, as recorded in the ODB
        """
        self.run_number = None
        self.run_start_time = None
        self.num_x_steps = 0
        self.num_y_steps = 0
        self.scan_ppg_x = []
        self.scan_ppg_y = []
        self.scan_epics_x = []
        self.scan_epics_y = []
        self.scan_lvv_x = []
        self.scan_lvv_y = []
        self.scan_dac_x = []
        self.scan_dac_y = []
        self.dac_defaults = []
        self.flip_bank_order = []
        self.flip_state_meanings = {}
        self.ppg_mute_polarity = {}
        self.mca4_bin_width_ns = 0
        self.sr430_bin_width_ns = 0
        self.constant_scope_background_uVsecs = None
        self.scope_width_secs = None
        self.electron_area_factor = None
        self.ppg_program = None

    def get_all_scanned_var_names(self):
        x_keys = []
        y_keys = []

        if self.num_x_steps > 0:
            x_keys.extend([p for p in self.scan_ppg_x if p != ""])
            x_keys.extend([p for p in self.scan_epics_x if p != ""])
            x_keys.extend([p for p in self.scan_lvv_x if p != ""])
            x_keys.extend([p for p in self.scan_dac_x if p != ""])

        if self.num_y_steps > 0:
            y_keys.extend([p for p in self.scan_ppg_y if p != ""])
            y_keys.extend([p for p in self.scan_epics_y if p != ""])
            y_keys.extend([p for p in self.scan_lvv_y if p != ""])
            y_keys.extend([p for p in self.scan_dac_y if p != ""])

        return (x_keys, y_keys)

class CPETEvent:
    """
    Stores data from a single PPG cycle.
    
    Members:
    
    * event_time (float) - UNIX timestamp when event was read out
    * loop_count (int) - Overall loop number (number of scan sweeps completed)
    * x_step (int) - X step of the current loop
    * y_step (int) - Y step of the current loop
    * ppg_scan_values (dict of {str: float}) - For PPG values that are being scanned, ODB path -> value
    * ppg_muted (dict of {str: boolean}) - Whether any PPG blocks were being automatically muted/unmuted
    * ppg_mute_state (bool) - Low-level lookup of the polarity of the muting toggle
    * epics_scan_values (dict of {str: float}) - For EPICS values that are being scanned, device name -> value
    * lvv_scan_values (dict of {str: float}) - For LabView Voltage parameters that are being scanned, param -> value
    * dac_scan_values (dict of {str: float}) - For CPET DAC values that are being scanned, ODB path -> value
    * dac_voltages (list of float) - DAC channel voltages
    * mca4_chan1 (list of int) - Histogram from STOP1 of MCA4
    * mca4_chan2 (list of int) - Histogram from STOP2 of MCA4
    * sr430 (list of int) - Histogram from SR430
    * scope_area_uVsecs (float) - Raw value of MCP signal from the LeCroy scope in uV * secs
    * scope_background_uVsecs (float) - Baseline of MCP signal from the LeCroy scope multiplied by time
        window over which the electron signal was integrated
    * electron_signal (float) - Calculated electron signal from MCP (after subtracting from background 
        and converting from uV * secs to number of electrons)
    * electron_signal_nosub (float) - Calculated electron signal from MCP (after only converting from 
        uV * secs to number of electrons - no background subtractions)
    * original_midas_event (`midas.event.MidasEvent`)
    """
    def __init__(self):
        self.event_time = None
        
        self.loop_count = 0
        self.x_step = 0
        self.y_step = 0
        
        self.ppg_scan_values = {} 
        self.ppg_muted = {}
        self.epics_scan_values = {}
        self.lvv_scan_values = {}
        self.dac_scan_values = {}
        self.dac_voltages = []
        
        self.ppg_mute_state = False
        
        self.mca4_chan1 = []
        self.mca4_chan2 = []
        self.sr430 = []
        
        self.scope_area_uVsecs = None
        self.scope_background_uVsecs = None
        self.electron_signal = None
        self.electron_signal_nosub = None
        
        self.original_midas_event = None
        
    def get_all_scanned_vars(self):
        scan_vals = self.ppg_scan_values
        scan_vals.update(self.epics_scan_values)
        scan_vals.update(self.lvv_scan_values)
        scan_vals.update(self.dac_scan_values)
        return scan_vals

    def dump_to_screen(self):
        print("Event # %s, at %s" % (self.original_midas_event.header.serial_number, datetime.datetime.fromtimestamp(self.event_time)))
        print("  Loop count %d, X point %d, Y point %d" % (self.loop_count, self.x_step, self.y_step))
        
        if len(self.ppg_scan_values):
            print("  PPG scanning:")
            for k, v in self.ppg_scan_values.items():
                print("    %s => %s" % (k, v))
        if len(self.ppg_muted):
            print("  PPG muting:")
            for k, v in self.ppg_muted.items():
                print("    %s => %s" % (k, "Muted" if v else "Unmuted"))
        if len(self.epics_scan_values):
            print("  EPICS scanning:")
            for k, v in self.epics_scan_values.items():
                print("    %s => %s" % (k, v))
        if len(self.lvv_scan_values):
            print("  LabView Voltage scanning:")
            for k, v in self.lvv_scan_values.items():
                print("    %s => %s" % (k, v))
        if len(self.dac_voltages):
            print("  DAC voltages: %s" % self.dac_voltages)
                
        max_val_chan1 = 0
        max_val_chan2 = 0
        max_val_sr430 = 0
        max_bin_chan1 = None
        max_bin_chan2 = None
        max_bin_sr430 = None
        
        for i, v in enumerate(self.mca4_chan1):
            if v > max_val_chan1:
                max_bin_chan1 = i
                max_val_chan1 = v
            
        for i, v in enumerate(self.mca4_chan2):
            if v > max_val_chan2:
                max_bin_chan2 = i
                max_val_chan2 = v
            
        for i, v in enumerate(self.sr430):
            if v > max_val_sr430:
                max_bin_sr430 = i
                max_val_sr430 = v
        
        print("  MCA4 channel 1 has %s bins. Max bin is %s with %s counts" % (len(self.mca4_chan1), max_bin_chan1, max_val_chan1))
        print("  MCA4 channel 2 has %s bins. Max bin is %s with %s counts" % (len(self.mca4_chan2), max_bin_chan2, max_val_chan2))
        print("  SR430 has %s bins. Max bin is %s with %s counts" % (len(self.sr430), max_bin_sr430, max_val_sr430))
            
        
def get_cpet_run_info(odb_json):
    """
    Get the current scan configuration from the provided ODB.
    
    Args:
        
    * odb_json (dict) - ODB dump
    
    Returns:
        `CPETRunInfo`
    """
    run_info = CPETRunInfo()
    run_info.run_number = odb_json["Runinfo"]["Run number"]
    run_info.run_start_time = int(odb_json["Runinfo"]["Start time binary"], 16)
    run_info.ppg_program = odb_json["Equipment"]["PPGCompiler"]["Programming"]
    
    x_enabled = odb_json["Scanning"]["Global"]["Settings"]["Enable X"]
    y_enabled = odb_json["Scanning"]["Global"]["Settings"]["Enable Y"]

    run_info.num_x_steps = odb_json["Scanning"]["Global"]["Settings"]["nX steps"] if x_enabled else 0
    run_info.num_y_steps = odb_json["Scanning"]["Global"]["Settings"]["nY steps"] if y_enabled else 0
    
    epics_settings = odb_json["Scanning"]["Epics"]["Settings"]
    epics_dev_list = odb_json["Scanning"]["Epics"]["Definitions"]["Demand device"]
    
    clients_running = [c["Name"] for c in odb_json["System"]["Clients"].values()]
    lvv_running = "fe_labview_voltages" in clients_running
    dac_running = "fe_dac" in clients_running
    daq_controls_dac = dac_running and odb_json["Equipment"]["DAC"]["Settings"]["Enable"]
    
    if daq_controls_dac:
        run_info.dac_defaults = odb_json["Equipment"]["DAC"]["Computed"]["Voltages"]
    else:
        run_info.dac_defaults = []
    
    if x_enabled:
        run_info.scan_ppg_x = odb_json["Scanning"]["PPG"]["Settings"]["X paths"]
        run_info.scan_epics_x = [epics_dev_list[idx] if idx >= 0 else "" for idx in epics_settings["X devices"] ]
        run_info.scan_lvv_x = odb_json["Equipment"]["LabViewVoltages"]["Settings"]["X keys"] if lvv_running else []
        run_info.scan_dac_x = odb_json["Equipment"]["DAC"]["Settings"]["X path"] if daq_controls_dac else []
    else:
        run_info.scan_ppg_x = []
        run_info.scan_epics_x = []
        run_info.scan_lvv_x = []
        run_info.scan_dac_x = []
    
    if y_enabled:
        run_info.scan_ppg_y = odb_json["Scanning"]["PPG"]["Settings"]["Y paths"]
        run_info.scan_epics_y = [epics_dev_list[idx] if idx >= 0 else "" for idx in epics_settings["Y devices"] ]
        run_info.scan_lvv_y = odb_json["Equipment"]["LabViewVoltages"]["Settings"]["Y keys"] if lvv_running else []
        run_info.scan_dac_y = odb_json["Equipment"]["DAC"]["Settings"]["Y path"] if daq_controls_dac else []
    else:
        run_info.scan_ppg_y = []
        run_info.scan_epics_y = []
        run_info.scan_lvv_y = []
        run_info.scan_dac_y = []
            
    mca4_units = int(odb_json["Equipment"]["MCA4"]["Settings"]["Bin width units"], 16) # (ns, us, ms, s) for (0, 1, 2, 3)
    mca4_width = odb_json["Equipment"]["MCA4"]["Settings"]["Time bin width"]
     
    if mca4_units == 0:
        run_info.mca4_bin_width_ns = mca4_width
    elif mca4_units == 1:
        run_info.mca4_bin_width_ns = mca4_width * 1e3
    elif mca4_units == 2:
        run_info.mca4_bin_width_ns = mca4_width * 1e6
    elif mca4_units == 3:
        run_info.mca4_bin_width_ns = mca4_width * 1e9
        
    try:
        sr430_width_code = odb_json["Equipment"]["SR430"]["Settings"]["Bin width code"]
        
        run_info.sr430_bin_width_ns = {
          0: 5,
          1: 40,
          2: 80,
          3: 160,
          4: 320,
          5: 640,
          6: 1.28e3,
          7: 2.56e3,
          8: 5.12e3,
          9: 10.24e3,
          10: 20.48e3,
          11: 40.96e3,
          12: 81.92e3,
          13: 163.84e3,
          14: 327.68e3,
          15: 655.36e3,
          16: 1.3107e6,
          17: 2.6214e6,
          18: 5.2429e6,
          19: 10.486e6  
        }[sr430_width_code]
    except KeyError:
        run_info.sr430_bin_width_ns = None
               
    try:
        if odb_json["Equipment"]["WaveSurfer3504"]["Settings"]["Subtract from fixed background"]:
            run_info.constant_scope_background_uVsecs = odb_json["Equipment"]["WaveSurfer3504"]["Settings"]["Fixed background (uV s)"]
    except KeyError:
        pass
        
    try:
        run_info.electron_area_factor = odb_json["Equipment"]["WaveSurfer3504"]["Settings"]["Area conversion (e- per uV s)"]
    except KeyError:
        run_info.electron_area_factor = 6.242e07
        
    scope_cfg = odb_json["Equipment"]["WaveSurfer3504"]["Settings"]["Scope config"]
    
    # Parse e.g. "300 us" into an actual time division in seconds
    for i,c in enumerate(scope_cfg["Time div"]):
        if not c.isdigit():
            break

    number = float(scope_cfg["Time div"][:i])
    unit = scope_cfg["Time div"][i:].strip().lower()
    factors = {"s": 1, "ms": 1e-3, "us": 1e-6, "ns": 1e-9}
    time_div_secs = number * factors[unit]

    run_info.scope_width_secs = (float(scope_cfg["P1 gate stop"]) - float(scope_cfg["P1 gate start"])) * time_div_secs

    try:
        run_info.flip_bank_order = odb_json["Scanning"]["Global"]["Computed"]["FLIP bank order"]
    except KeyError:
        run_info.flip_bank_order = []
    
    if not isinstance(run_info.flip_bank_order, list):
        run_info.flip_bank_order = [run_info.flip_bank_order]
        
    if len(run_info.flip_bank_order) == 1 and run_info.flip_bank_order[0] == "N/A":
        run_info.flip_bank_order = []
        
    for flip_device in run_info.flip_bank_order:
        try:
            true_meaning = odb_json["Scanning"][flip_device]["Computed"]["True meaning"]
        except KeyError:
            true_meaning = "True"
        try:
            false_meaning = odb_json["Scanning"][flip_device]["Computed"]["False meaning"]
        except KeyError:
            false_meaning = "False"
        
        run_info.flip_state_meanings[flip_device] = {True: true_meaning, False: false_meaning}
                
    if "FlipMutingPPG" in odb_json["Scanning"]:
        mute_settings = odb_json["Scanning"]["FlipMutingPPG"]["Settings"]
        
        if mute_settings["Enable"]:
            for i, name in enumerate(mute_settings["Block names"]):
                if name != "":
                    run_info.ppg_mute_polarity[name] = mute_settings["Mute then unmute"][i]
                
    return run_info

def get_cpet_event_info(midas_event, run_info):
    """
    Get scan setup for this event.
    
    Args:
        
    * midas_event (`midas.event.MidasEvent`)
    * run_info (`CPETRunInfo`)
    
    Returns:
        CPETEvent
    """
    cpet_event = CPETEvent()
    
    if not midas_event.bank_exists("SCAN"):
        return None
    
    cpet_event.original_midas_event = midas_event
    
    if midas_event.bank_exists("ETMS"):
        # Get timestamp in ms and convert to s
        cpet_event.event_time = midas_event.banks["ETMS"].data[0] / 1000.
    else:
        # Get timestamp in s
        cpet_event.event_time = midas_event.header.timestamp
    
    # Scan metadata
    cpet_event.loop_count = midas_event.banks["SCAN"].data[0]
    cpet_event.x_step = midas_event.banks["SCAN"].data[1]
    cpet_event.y_step = midas_event.banks["SCAN"].data[2]
       
    if midas_event.bank_exists("XPPG"):
        for i, path in enumerate(run_info.scan_ppg_x):
            if path != "" and path is not None:
                cpet_event.ppg_scan_values[path] = midas_event.banks["XPPG"].data[i]
                
    if midas_event.bank_exists("YPPG"):
        for i, path in enumerate(run_info.scan_ppg_y):
            if path != "" and path is not None:
                cpet_event.ppg_scan_values[path] = midas_event.banks["YPPG"].data[i]
        
    if midas_event.bank_exists("XEPD"):
        for i, dev in enumerate(run_info.scan_epics_x):
            if dev != "" and dev is not None:
                cpet_event.epics_scan_values[dev] = midas_event.banks["XEPD"].data[i]
                
    if midas_event.bank_exists("YEPD"):
        for i, dev in enumerate(run_info.scan_epics_y):
            if dev != "" and dev is not None:
                cpet_event.epics_scan_values[dev] = midas_event.banks["YEPD"].data[i]
                
    if midas_event.bank_exists("XLVV"):
        for i, path in enumerate(run_info.scan_lvv_x):
            if path != "" and path is not None:
                cpet_event.lvv_scan_values[path] = midas_event.banks["XLVV"].data[i]
                
    if midas_event.bank_exists("YLVV"):
        for i, path in enumerate(run_info.scan_lvv_y):
            if path != "" and path is not None:
                cpet_event.lvv_scan_values[path] = midas_event.banks["YLVV"].data[i]
                
    if midas_event.bank_exists("XDAC"):
        for i, path in enumerate(run_info.scan_dac_x):
            if path != "" and path is not None:
                cpet_event.dac_scan_values[path] = midas_event.banks["XDAC"].data[i]
                
    if midas_event.bank_exists("YDAC"):
        for i, path in enumerate(run_info.scan_dac_y):
            if path != "" and path is not None:
                cpet_event.dac_scan_values[path] = midas_event.banks["YDAC"].data[i]
                
    if midas_event.bank_exists("DACV"):
        cpet_event.dac_voltages = midas_event.banks["DACV"].data
    else:
        cpet_event.dac_voltages = run_info.dac_defaults
    
    if midas_event.bank_exists("MCS1"):
        cpet_event.mca4_chan1 = midas_event.banks["MCS1"].data
        
    if midas_event.bank_exists("MCS2"):
        cpet_event.mca4_chan2 = midas_event.banks["MCS2"].data
        
    if midas_event.bank_exists("S430"):
        cpet_event.sr430 = midas_event.banks["S430"].data
        
    if midas_event.bank_exists("WS35"):
        cpet_event.scope_area_uVsecs = midas_event.banks["WS35"].data[0]

        if run_info.constant_scope_background_uVsecs is not None:
            cpet_event.scope_background_uVsecs = run_info.constant_scope_background_uVsecs
        elif len(midas_event.banks["WS35"].data) > 1:
            # mV * secs to uV * secs (uWb)
            cpet_event.scope_background_uVsecs = midas_event.banks["WS35"].data[1] * run_info.scope_width_secs * 1e3
        else:
            cpet_event.scope_background_uVsecs = 0
        
    flip_states = {}
    
    if midas_event.bank_exists("FLIP"):
        if len(midas_event.banks["FLIP"].data) != len(run_info.flip_bank_order):
            raise ValueError("Unexpected number of flippable devices: %d/%d" % (len(midas_event.banks["FLIP"].data), len(run_info.flip_bank_order)))
        
        for i, state in enumerate(midas_event.banks["FLIP"].data):
            dev = run_info.flip_bank_order[i]
            flip_states[dev] = (state, run_info.flip_state_meanings[dev][state])
    
    if "FlipMutingPPG" in flip_states:
        cpet_event.ppg_mute_state = flip_states["FlipMutingPPG"][0]
        
        for block_name, polarity in run_info.ppg_mute_polarity.items():
            cpet_event.ppg_muted[block_name] = (cpet_event.ppg_mute_state == polarity)
    else:
        cpet_event.ppg_mute_state = False

    
    if cpet_event.scope_area_uVsecs is not None:       
        cpet_event.electron_signal_nosub = cpet_event.scope_area_uVsecs * run_info.electron_area_factor
        cpet_event.electron_signal = (cpet_event.scope_area_uVsecs - cpet_event.scope_background_uVsecs) * run_info.electron_area_factor
    
    return cpet_event
