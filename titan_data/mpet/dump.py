"""
Tool that will print MPET data to screen.

If an argument is provided, we open that midas file.
If no argument is provided, we connect to the live midas experiment.
"""

try:
    import midas
    import midas.client
    got_client = True
except (ImportError, SyntaxError, EnvironmentError):
    import titan_data.midas as midas
    got_client = False
    
import titan_data.mpet
import argparse

def handle_event(mpet_event, as_csv, just_vt2=False):
    if as_csv:
        for trig in mpet_event.caen_tdc_parsed:
            if len(trig.pos_x_mm) == 1 and not trig.failed_to_compute_positions:
                print("%d,%d,%f,%f,%f,%f,%f,%f,%f" % (  mpet_event.event_time,
                                                        trig.trigger_count, 
                                                        trig.all_tof_secs[1][0] * 1e6, 
                                                        trig.all_tof_secs[2][0] * 1e6, 
                                                        trig.all_tof_secs[3][0] * 1e6, 
                                                        trig.all_tof_secs[4][0] * 1e6, 
                                                        trig.all_tof_secs[5][0] * 1e6, 
                                                        trig.pos_x_mm[0], 
                                                        trig.pos_y_mm[0]))
    elif just_vt2:
        print("Event %d has %s timestamps and %s positions" % (mpet_event.serial_number, len(mpet_event.vt2_data), len(mpet_event.pos_data)))
        print("  %s VT2 TDC entries:" % len(mpet_event.vt2_data))
        print("  %s VT2 TDC gate_start entries:" % len(list(filter(lambda x: x.event_type == 1,mpet_event.vt2_data))))
        print("  %s VT2 TDC gate_end entries:" % len(list(filter(lambda x: x.event_type == 2,mpet_event.vt2_data))))
        print("  %s VT2 TDC new_cycle entries:" % len(list(filter(lambda x: x.event_type == 3,mpet_event.vt2_data))))
        print("  %s VT2 TDC timpestamp entries:" % len(list(filter(lambda x: x.event_type == 4,mpet_event.vt2_data))))
    else:
        mpet_event.dump_to_screen()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Dump MPET data to screen')
    file_help = "Midas file must be provided"
    file_nargs = 1
    if got_client:
        file_help = "If midas file not provided, connects to live experiment"
        file_nargs = '?'
    parser.add_argument('filename', metavar="midas_file", nargs=file_nargs, help=file_help)
    parser.add_argument('--caen-as-csv', action='store_true', help="Dump reconstructed CAEN data as CSV")
    parser.add_argument('--just-vt2', action='store_true', help="Only dump VT2 data for each event")
    args = parser.parse_args()
    
    if args.caen_as_csv:
        print("event_time,trigger_id,time_fast_us,time_x1_us,time_x2_us,time_y1_us,time_y2_us,pos_x_mm,pos_y_mm")

    if args.filename is not None:
        # Print from file
        file_path = args.filename[0] if file_nargs == 1 else args.filename

        reader = titan_data.mpet.MPETFile(file_path)
        
        if not args.caen_as_csv:
            reader.run_info.dump_to_screen()
    
        for mpet_event in reader:
            handle_event(mpet_event, args.caen_as_csv, args.just_vt2)
    else:
        # Print live events
        if not got_client:
            print("Midas must be installed (and the full midas python packages available) for live events to be dumped")
            exit(0)
        
        client = midas.client.MidasClient("mpetdump")
        buffer_handle = client.open_event_buffer("SYSTEM")
        client.register_event_request(buffer_handle, sampling_type=midas.GET_ALL)
        
        curr_run = None
        
        while True:
            # Reload run-level settings if needed
            new_run = client.odb_get("/Runinfo/Run number")
            
            if new_run != curr_run:
                run_info = titan_data.mpet.get_mpet_run_info(client.odb_get("/"))
                curr_run = new_run
            
            # Handle any events
            midas_event = client.receive_event(buffer_handle, True)
            
            if midas_event:
                mpet_event = titan_data.mpet.get_mpet_event_info(midas_event, run_info)
                
                if mpet_event:
                    handle_event(mpet_event, args.caen_as_csv, args.just_vt2)
                
            client.communicate(100)
            
