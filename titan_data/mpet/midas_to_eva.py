"""
This script converts MPET data into Eva files.

Documentation on Eva can be found at https://groups.nscl.msu.edu/lebit/downloads/index.html.

Documentation for this script can be found on the TITAN wiki under Documentation/MPET/Upated MIDAS to Eva Conversion Script

Eva does not support storing other metadata (e.g. position) in the Eva file.  Use PhIAT or another similar program for position data.

While earlier versions of this script only interpreted frequency scans, this version has been updated to accomodate other scans
including EPICS, PPG, and Softdac/16AO16 (trap voltage) variables.

Data from 3 different TDCs can be used: Caen TDC (V1290), VT4, and VT2.
Default is to use V1290 data if it is enabled, VT4 data if V1290 is not enabled, and VT2 data if V1290 and VT4 are both not enabled.
User can override this default using "-t" from command line.
"""
import titan_data.mpet
import os.path
import argparse
import struct
import datetime
import math
import numpy
import enum
import statistics

class MPETConfig:
    """
    Structure for holding configuration about a run, some of
    which is written to the Eva file. Generally this is populated
    from the ODB dump at the start of each midas file, but the
    user can override some values with command-line arguments.
    
    Members:
        
    * afg_version (str) - "AFG" for Agilent, "AFGTek" for Tektronix
    * species (str) - e.g. 1Li6
    * charge (float) - Ion charge
    * num_freq_steps (int) - Number of frequency steps
    * rf_amplitude (float) - RF amplitude in V
    * rf_time_secs (float) - RF on time
    * tdc_gate_secs (float) - How long the TDC gate was open for.
    * n_tdc_bins (int) - Number of bins in each histogram
    * run_start_time (int) - UNIX timestamp of run start
    * run_end_time (int) - UNIX timestamp of run end
    * num_x_steps (int) - If scanning voltages/EPICS etc, number of X steps
    * num_y_steps (int) - If scanning voltages/EPICS etc, number of Y steps
    * num_z_steps (int) - If scanning voltages/EPICS etc, number of Z steps
    * num_reps_at_step (int) - Number of times we repeat X/Y step before moving on
    * TDCChoice (str) - V1290, VT4, or VT2 accepted
    * AFG_scan_type (member of `FreqScan` enum)
    """
    def __init__(self):
        self.afg_version = None
        self.species = None
        self.charge = None
        self.num_freq_steps = {None: 0}
        self.rf_amplitude = None
        self.rf_time_secs = None 
        self.tdc_gate_secs = None
        self.n_tdc_bins = None
        self.run_start_time = None
        self.run_end_time = None
        self.num_x_steps = None
        self.num_y_steps = None
        self.num_z_steps = None
        self.num_reps_at_step = None
        self.TDCChoice = None
        self.AFG_scan_dev = None
        self.AFG_scan_type = FreqScan.NoScan

class EVAConfig:
    def __init__(self):
        self.dev = "NA"
        self.fct = "NA"
        self.start = 0
        self.stop = 0
        self.step = 1
        self.unit = "NA"
        self.values = [0]

class ScanPointData:
    """
    Simple structure for holding timestamp data for each scan X/Y point.
    
    Members:
        
    * binned_data (list of events at the given x/y, [E1, E2, ...] where each E is itselsf a list of shots of the form [Timestamp, {int: int}] - dict is {bin_number: count})
    * position_data (not used here, but kept from previous version in case future users would like to make this script do anything with position data)
    """
    def __init__(self):
        self.binned_data = []
        self.position_data = []

class FreqScan(enum.Enum):
    NoScan = 1
    InPPGLoop = 2
    XScan = 3
    YScan = 4
    ZScan = 5

class MidasToEva:
    """
    Main class with tools for parsing midas file and writing Eva files.
    
    Members:
        
    * input_file_path (str) - Midas file path
    * mpet_file (`data_format.MPETFile`) - MPET midas file access
    * config (`MPETConfig`)
    * data (dict of {int: {int: {`ScanPointData`}}}) - Keys are X step, Y step.
    """
    def __init__(self, path):
        self.input_file_path = path
        self.mpet_file = titan_data.mpet.MPETFile(path)
        constants = titan_data.mpet.MPETConstants()

        self.eor_odb = self.mpet_file.midas_file.get_eor_odb_dump() 
        self.bor_odb = self.mpet_file.midas_file.get_bor_odb_dump()
        self.run_info = self.mpet_file.run_info

        self.final_event_ts = 0

        self.num_full_loops = 0
        self.only_write_full_loops = False

        # EVA can only handle 200 scan points in the main scan dimension!
        self.max_eva_scan_length = 200

    def populate_config(self, args):
        """
        Extract information from BOR/EOR ODB dumps, and any items that the
        user has overridden on the command-line.

        Allow user to override certain settings in `self.config`.
        
        Args:
            
        * args (object from calling `argparse.parse_args()`)
        """
        self.config = MPETConfig()
        self.config.afg_version = None
        
        if "AFGTek" in self.bor_odb.data["Equipment"]:
            self.config.afg_version = "AFGTek"
        elif "AFG" in self.bor_odb.data["Equipment"]:
            self.config.afg_version = "AFG"
        else:
            self.config.afg_version = None

        if args.only_full_loops:
            self.only_write_full_loops = True

        if args.elem is None:
            if self.config.afg_version is None:
                raise ValueError("Use -e flag to specify which element is being studied")
            else:
                self.config.species = self.bor_odb.data["Equipment"][self.config.afg_version]["Settings"]["Species"]
        else:
            self.config.species = args.elem
        
        if args.z is None:
            if self.config.afg_version is None:
                raise ValueError("Use -z flag to specify charge")
            else:
                self.config.charge = self.bor_odb.data["Equipment"][self.config.afg_version]["Settings"]["Charge"]
        else:
            self.config.charge = str(args.z)

        if self.config.afg_version is None:
            self.config.IsQuadAFGOn = False
            self.config.IsDipoleAFGOn = False
        else:
            self.config.IsQuadAFGOn = self.bor_odb.data["Equipment"][self.config.afg_version]["Settings"]["Quad"]["Enabled"]
            self.config.IsDipoleAFGOn = self.bor_odb.data["Equipment"][self.config.afg_version]["Settings"]["Dipole"]["Enabled"]

        if self.mpet_file.run_info.freq_scanned_in_ppg:
            # Older AFG - scan in PPG loop not as X/Y var
            if self.config.IsQuadAFGOn and self.config.IsDipoleAFGOn:
                self.config.AFG_scan_dev = "Combo_Quad/Dipole_AFG"
                self.config.AFG_scan_type = FreqScan.InPPGLoop
                self.config.num_freq_steps["Combo_Quad/Dipole_AFG"] = self.bor_odb.data["Equipment"]["PPGCompiler"]["Programming"]["begin_ramp"]["loop count"]
            elif self.config.IsQuadAFGOn:
                self.config.AFG_scan_dev = "Quad_AFG"
                self.config.AFG_scan_type = FreqScan.InPPGLoop
                self.config.num_freq_steps["Quad_AFG"] = self.bor_odb.data["Equipment"]["PPGCompiler"]["Programming"]["begin_ramp"]["loop count"]
            elif self.config.IsDipoleAFGOn:
                self.config.AFG_scan_dev = "Dipole_AFG"
                self.config.AFG_scan_type = FreqScan.InPPGLoop
                self.config.num_freq_steps["Dipole_AFG"] = self.bor_odb.data["Equipment"]["PPGCompiler"]["Programming"]["begin_ramp"]["loop count"]
            else:
                self.config.AFG_scan_type = FreqScan.NoScan
        else:
            # New (Tektronix) AFG - scan like any other variable
            # Doesn't handle scanning Quad/Dipole as separate X/Y scans, but that
            # is no a use case MPET normally have.
            dim_q = self.bor_odb.data["Scanning"][self.config.afg_version]["Frequency"]["Quad"]["Dimension"].lower()
            dim_d = self.bor_odb.data["Scanning"][self.config.afg_version]["Frequency"]["Dipole"]["Dimension"].lower()
            
            if self.config.IsQuadAFGOn and self.config.IsDipoleAFGOn and dim_q == dim_d and dim_q != "none":
                self.config.AFG_scan_dev = "Combo_Quad/Dipole_AFG"
    
                if dim_q == "x":
                    self.config.AFG_scan_type = FreqScan.XScan
                    self.config.num_freq_steps["Combo_Quad/Dipole_AFG"] = self.bor_odb.data["Scanning"]["Global"]["Settings"]["nX steps"] + 1
                elif dim_q == "y":
                    self.config.AFG_scan_type = FreqScan.YScan
                    self.config.num_freq_steps["Combo_Quad/Dipole_AFG"] = self.bor_odb.data["Scanning"]["Global"]["Settings"]["nY steps"] + 1
                elif dim_q == "z":
                    self.config.AFG_scan_type = FreqScan.ZScan
                    self.config.num_freq_steps["Combo_Quad/Dipole_AFG"] = self.bor_odb.data["Scanning"]["Global"]["Settings"]["nZ steps"] + 1
                elif dim_q == "none":
                    self.config.AFG_scan_type = FreqScan.NoScan
                    self.config.num_freq_steps["Combo_Quad/Dipole_AFG"] = 1
                else:
                    raise ValueError("Unexpected value for /Scanning/AFG/Frequency/Quad/Dimension")
            elif self.config.IsQuadAFGOn or self.config.IsDipoleAFGOn:
                if self.config.IsQuadAFGOn:
                    self.config.AFG_scan_dev = "Quad_AFG"

                    if dim_q == "x":
                        self.config.AFG_scan_type = FreqScan.XScan
                        self.config.num_freq_steps["Quad_AFG"] = self.bor_odb.data["Scanning"]["Global"]["Settings"]["nX steps"] + 1
                    elif dim_q == "y":
                        self.config.AFG_scan_type = FreqScan.YScan
                        self.config.num_freq_steps["Quad_AFG"] = self.bor_odb.data["Scanning"]["Global"]["Settings"]["nY steps"] + 1
                    elif dim_q == "z":
                        self.config.AFG_scan_type = FreqScan.ZScan
                        self.config.num_freq_steps["Quad_AFG"] = self.bor_odb.data["Scanning"]["Global"]["Settings"]["nZ steps"] + 1
                    elif dim_q == "none":
                        self.config.AFG_scan_type = FreqScan.NoScan
                        self.config.num_freq_steps["Quad_AFG"] = 1
                    else:
                        raise ValueError("Unexpected value for /Scanning/AFG/Frequency/Quad/Dimension")
                elif self.config.IsDipoleAFGOn:
                    self.config.AFG_scan_dev = "Dipole_AFG"

                    if dim_d == "x":
                        self.config.AFG_scan_type = FreqScan.XScan
                        self.config.num_freq_steps["Dipole_AFG"] = self.bor_odb.data["Scanning"]["Global"]["Settings"]["nX steps"] + 1
                    elif dim_d == "y":
                        self.config.AFG_scan_type = FreqScan.YScan
                        self.config.num_freq_steps["Dipole_AFG"] = self.bor_odb.data["Scanning"]["Global"]["Settings"]["nY steps"] + 1
                    elif dim_d == "z":
                        self.config.AFG_scan_type = FreqScan.ZScan
                        self.config.num_freq_steps["Dipole_AFG"] = self.bor_odb.data["Scanning"]["Global"]["Settings"]["nZ steps"] + 1
                    elif dim_d == "none":
                        self.config.AFG_scan_type = FreqScan.NoScan
                        self.config.num_freq_steps["Dipole_AFG"] = 1
                    else:
                        raise ValueError("Unexpected value for /Scanning/AFG/Frequency/Dipole/Dimension")
            else:
                # Neither AFG on
                self.config.AFG_scan_type = FreqScan.NoScan
                self.config.num_freq_steps[None] = 1

        if args.afg is not None:
            if args.afg not in ["dipole", "quad"]:
                raise ValueError("AFG choice not recognized, must be dipole or quad.")

            if self.config.AFG_scan_dev == "Combo_Quad/Dipole_AFG":
                if args.afg == "dipole":
                    self.config.AFG_scan_dev = "Dipole_AFG"
                if args.afg == "quad":
                    self.config.AFG_scan_dev = "Quad_AFG"

            if self.config.AFG_scan_dev == "Dipole_AFG" and args.afg == "quad":
                raise ValueError("Quad AFG was not scanned.  Using dipole instead.")
            
            if self.config.AFG_scan_dev == "Quad_AFG" and args.afg == "dipole":
                raise ValueError("Dipole AFG was not scanned.  Using quad instead.")

            if self.config.AFG_scan_dev is None:
                raise ValueError("Cannot use specified AFG because no AFG was scanned.")

        if self.config.AFG_scan_dev == "Combo_Quad/Dipole_AFG":
            self.config.freq_list = list(range(0, max(self.config.num_freq_steps.values())))
            self.config.centre_freq = 0.000000
            self.config.rf_amplitude = 0.000000
            self.config.rf_time_secs = 0.000000
        elif self.config.AFG_scan_dev == "Quad_AFG":
            self.config.freq_list = self.eor_odb.data["Equipment"][self.config.afg_version]["Computed"]["Quad"]["Frequency list"]
            
            if len(self.config.freq_list) != self.config.num_freq_steps["Quad_AFG"]:
                print("Unexpected number of Quad frequencies in ODB. Re-computing based on first/last and number of steps")
                start = self.config.freq_list[0]
                end = self.config.freq_list[-1]
                gap = (end-start)/(self.config.num_freq_steps["Quad_AFG"]-1)
                self.config.freq_list = [start + gap * i for i in range(self.config.num_freq_steps["Quad_AFG"])]
            
            self.config.centre_freq = (self.config.freq_list[0] + self.config.freq_list[-1])/2
            self.config.rf_amplitude = self.eor_odb.data["Equipment"][self.config.afg_version]["Computed"]["Quad"]["Amplitude (V)"]
            self.config.rf_time_secs = self.eor_odb.data["Equipment"][self.config.afg_version]["Computed"]["Quad"]["Excitation time (ms)"] / 1000.
        elif self.config.AFG_scan_dev == "Dipole_AFG":
            self.config.freq_list = self.eor_odb.data["Equipment"][self.config.afg_version]["Computed"]["Dipole"]["Frequency list"]
            
            if len(self.config.freq_list) != self.config.num_freq_steps["Dipole_AFG"]:
                print("Unexpected number of Dipole frequencies in ODB. Re-computing based on first/last and number of steps")
                start = self.config.freq_list[0]
                end = self.config.freq_list[-1]
                gap = (end-start)/(self.config.num_freq_steps["Dipole_AFG"]-1)
                self.config.freq_list = [start + gap * i for i in range(self.config.num_freq_steps["Dipole_AFG"])]

            self.config.centre_freq = (self.config.freq_list[0] + self.config.freq_list[-1])/2
            self.config.rf_amplitude = self.eor_odb.data["Equipment"][self.config.afg_version]["Computed"]["Dipole"]["Amplitude (V)"]
            self.config.rf_time_secs = self.eor_odb.data["Equipment"][self.config.afg_version]["Computed"]["Dipole"]["Excitation time (ms)"] / 1000.
        else:
            self.config.AFG_scan_dev = None
            self.config.freq_list = [0]
            self.config.centre_freq = 0.000000
            self.config.rf_amplitude = 0.000000
            self.config.rf_time_secs = 0.000000
        
        if args.tdc is None:
            try:
                is_vt2_on = self.bor_odb.data["Scanning"]["VT2"]["Settings"]["Enable"]
            except KeyError:
                is_vt2_on = False

            try:
                is_vt4_on = self.bor_odb.data["Scanning"]["VT4"]["Settings"]["Enable"]
            except KeyError:
                is_vt4_on = False
            
            try:
                is_v1290_on = self.bor_odb.data["Scanning"]["V1290"]["Settings"]["Enable"]
            except KeyError:
                is_v1290_on = False

            if is_v1290_on:
                self.config.TDCChoice = "V1290"
            elif is_vt4_on:
                self.config.TDCChoice = "VT4"
            elif is_vt2_on:
                self.config.TDCChoice = "VT2"
            else: 
                raise ValueError("Couldn't detect which TDC was active during this run. If you know it, use the -t argument and try again.")
        elif args.tdc in ["V1290", "VT4", "VT2"]:
            self.config.TDCChoice = args.tdc
        else:
            raise ValueError("TDC choice not recognized, must be V1290, VT4, or VT2.")

        if args.tdc_gate_secs is not None:
            self.config.tdc_gate_secs = args.tdc_gate_secs
        elif self.config.TDCChoice == "V1290":
            if self.mpet_file.run_info.v1290_t0_offset_from_ppg_block_chosen == "multi_caen_trig":
                block = self.bor_odb.data["Equipment"]["PPGCompiler"]["Programming"]["multi_caen_trig"]
                width_ms = block["delay between reps (ms)"] * block["rep count"]
            else:
                # 50us width for V1290
                width_ms = 0.05

            self.config.tdc_gate_secs = (self.mpet_file.run_info.default_caen_t0_offset_ms + width_ms)/1000
        elif self.config.TDCChoice in ["VT2", "VT4"]:
            if "pulse_TDCGate" in self.bor_odb.data["Equipment"]["PPGCompiler"]["Programming"]:
                self.config.tdc_gate_secs = self.bor_odb.data["Equipment"]["PPGCompiler"]["Programming"]["pulse_TDCGate"]["pulse width (ms)"] / 1000.
            else:
                raise ValueError("Couldn't detect max TDC ToF from PPG program. Use the -g argument to specify it.")
        else:
            self.config.tdc_gate_secs = 0

        self.config.run_start_time = int(self.eor_odb.data["Runinfo"]["Start time binary"], 16)
        self.config.run_end_time = int(self.eor_odb.data["Runinfo"]["Stop time binary"], 16)

        if args.nbins is None:
            self.config.n_tdc_bins = 10000
        else:
            self.config.n_tdc_bins = args.nbins

        self.config.scan_settings = self.bor_odb.data["Scanning"]["Global"]["Settings"]
        
        if "Enable Z" not in self.config.scan_settings:
            self.config.scan_settings["Enable Z"] = False

        self.config.num_x_steps = 0
        self.config.num_y_steps = 0
        self.config.num_z_steps = 0
        self.config.num_reps_at_step = self.config.scan_settings["N cycles per step"]
        
        # Treat frequency scan as special, for backwards-compatibility with old data
        if self.config.scan_settings["Enable X"] and self.config.AFG_scan_type != FreqScan.XScan:
            self.config.num_x_steps = self.config.scan_settings["nX steps"]
        if self.config.scan_settings["Enable Y"] and self.config.AFG_scan_type != FreqScan.YScan:
            self.config.num_y_steps = self.config.scan_settings["nY steps"]
        if self.config.scan_settings["Enable Z"] and self.config.AFG_scan_type != FreqScan.ZScan:
            self.config.num_z_steps = self.config.scan_settings["nZ steps"]
        
        self.data = {}
        
        for x in range(self.config.num_x_steps + 1):
            self.data[x] = {}
            
            for y in range(self.config.num_y_steps + 1):
                self.data[x][y] = {}
            
                for z in range(self.config.num_z_steps + 1):
                    self.data[x][y][z] = ScanPointData()

        self.config.scannedXvarsPPG = []
        self.config.scannedYvarsPPG = []
        self.config.scannedZvarsPPG = []
        self.config.scannedXvarsEPICS = []
        self.config.scannedYvarsEPICS = []
        self.config.scannedZvarsEPICS = []
        self.config.scannedXvarsTrapVoltages = []
        self.config.scannedYvarsTrapVoltages = []
        self.config.scannedZvarsTrapVoltages = []
        self.config.scannedXvarsAfgVoltages = []
        self.config.scannedYvarsAfgVoltages = []
        self.config.scannedZvarsAfgVoltages = []
        self.config.scannedXvarsAfgFreqs = []
        self.config.scannedYvarsAfgFreqs = []
        self.config.scannedZvarsAfgFreqs = []

        if "GSC_16ao16" in self.bor_odb.data["Scanning"]:
            self.config.trapvDevice = "GSC_16ao16"
        elif "Softdac" in self.bor_odb.data["Scanning"]:
            self.config.trapvDevice = "Softdac"
        else:
            self.config.trapvDevice = None

        if self.config.trapvDevice is None:
            self.config.trapvChanNames = []
        else:
            self.config.trapvChanNames = self.bor_odb.data["Scanning"][self.config.trapvDevice]["Settings"]["Channel names"]

        x_scan = self.config.scan_settings["Enable X"] and self.config.AFG_scan_type != FreqScan.XScan
        y_scan = self.config.scan_settings["Enable Y"] and self.config.AFG_scan_type != FreqScan.YScan
        z_scan = self.config.scan_settings["Enable Z"] and self.config.AFG_scan_type != FreqScan.ZScan

        print("AFG scan type: %s" % self.config.AFG_scan_type)

        if x_scan:
            self.config.scannedXvarsPPG[:] = ["/".join(x.split('/')[-2:]) for x in self.run_info.scan_ppg_x if x != '']
            self.config.scannedXvarsEPICS[:] = [x for x in self.run_info.scan_epics_x if x != '']
            self.config.scannedXvarsTrapVoltages[:] = [x for x in self.run_info.scan_trapv_x if x[0] != '']
            self.config.scannedXvarsTrapVoltages = [x[0] + "_" + self.config.trapvChanNames[x[1]] for x in self.config.scannedXvarsTrapVoltages]
            self.config.scannedXvarsAfgVoltages[:] = self.run_info.scan_afg_volt_x
            self.config.scannedXvarsAfgFreqs[:] = self.run_info.scan_afg_freq_x

            if len(self.config.scannedXvarsPPG):
                print("scanned x ppg:")
                print(self.config.scannedXvarsPPG)

            if len(self.config.scannedXvarsEPICS):
                print("scanned x epics:")
                print(self.config.scannedXvarsEPICS)

            if len(self.config.scannedXvarsTrapVoltages):
                print("scanned x trap voltages")
                print(self.config.scannedXvarsTrapVoltages)

            if len(self.config.scannedXvarsAfgVoltages):
                print("scanned x AFG voltages")
                print(self.config.scannedXvarsAfgVoltages)

            if len(self.config.scannedXvarsAfgFreqs):
                print("scanned x AFG freqs")
                print(self.config.scannedXvarsAfgFreqs)

        if y_scan:
            self.config.scannedYvarsPPG[:] = ["/".join(x.split('/')[-2:]) for x in self.run_info.scan_ppg_y if x != '']
            self.config.scannedYvarsEPICS[:] = [x for x in self.run_info.scan_epics_y if x != '']
            self.config.scannedYvarsTrapVoltages[:] = [x for x in self.run_info.scan_trapv_y if x[0] != '']
            self.config.scannedYvarsTrapVoltages = [x[0] + "_" + self.config.trapvChanNames[x[1]] for x in self.config.scannedYvarsTrapVoltages]
            self.config.scannedYvarsAfgVoltages[:] = self.run_info.scan_afg_volt_y
            self.config.scannedYvarsAfgFreqs[:] = self.run_info.scan_afg_freq_y

            if len(self.config.scannedYvarsPPG):
                print("scanned y ppg:")
                print(self.config.scannedYvarsPPG)

            if len(self.config.scannedYvarsEPICS):
                print("scanned y epics:")
                print(self.config.scannedYvarsEPICS)

            if len(self.config.scannedYvarsTrapVoltages):
                print("scanned y trap voltages")
                print(self.config.scannedYvarsTrapVoltages)

            if len(self.config.scannedYvarsAfgVoltages):
                print("scanned y AFG voltages")
                print(self.config.scannedYvarsAfgVoltages)

            if len(self.config.scannedYvarsAfgFreqs):
                print("scanned y AFG freqs")
                print(self.config.scannedYvarsAfgFreqs)

        if z_scan:
            self.config.scannedZvarsPPG[:] = ["/".join(x.split('/')[-2:]) for x in self.run_info.scan_ppg_z if x != '']
            self.config.scannedZvarsEPICS[:] = [x for x in self.run_info.scan_epics_z if x != '']
            self.config.scannedZvarsTrapVoltages[:] = [x for x in self.run_info.scan_trapv_z if x[0] != '']
            self.config.scannedZvarsTrapVoltages = [x[0] + "_" + self.config.trapvChanNames[x[1]] for x in self.config.scannedZvarsTrapVoltages]
            self.config.scannedZvarsAfgVoltages[:] = self.run_info.scan_afg_volt_z
            self.config.scannedZvarsAfgFreqs[:] = self.run_info.scan_afg_freq_z

            if len(self.config.scannedZvarsPPG):
                print("scanned z ppg:")
                print(self.config.scannedZvarsPPG)

            if len(self.config.scannedZvarsEPICS):
                print("scanned z epics:")
                print(self.config.scannedZvarsEPICS)

            if len(self.config.scannedZvarsTrapVoltages):
                print("scanned z trap voltages")
                print(self.config.scannedZvarsTrapVoltages)

            if len(self.config.scannedZvarsAfgVoltages):
                print("scanned z AFG voltages")
                print(self.config.scannedZvarsAfgVoltages)

            if len(self.config.scannedZvarsAfgFreqs):
                print("scanned z AFG freqs")
                print(self.config.scannedZvarsAfgFreqs)

        # Deal with cases where TDC gate is varied over the course of the scan
        ppg_scan = {
            "X": self.config.scannedXvarsPPG, 
            "Y": self.config.scannedYvarsPPG, 
            "Z": self.config.scannedZvarsPPG
        }

        if self.config.TDCChoice == "V1290":
            for (dim, scan) in ppg_scan.items():
                if "pulse_caen_end_trig_window/time offset (ms)" in scan:
                    print("Caen TDC trigger delay is being %s scanned, so TDC histogram window in Eva is not consistent.  Using largest trigger delay for histogram window.  TOF will still be accurate relative to the reference pulse.", dim)
                    self.config.tdc_gate_secs = self.get_max_ppg_scan_val(self.bor_odb, scan, "pulse_caen_end_trig_window/time offset (ms)", dim)
                elif "multi_caen_trig/time offset (ms)" in scan:
                    print("Caen TDC trigger delay is being %s scanned, so TDC histogram window in Eva is not consistent.  Using largest trigger delay for histogram window.  TOF will still be accurate relative to the reference pulse.", dim)
                    self.config.tdc_gate_secs = self.get_max_ppg_scan_val(self.bor_odb, scan, "multi_caen_trig/time offset (ms)", dim)
        else:
            for (dim, scan) in ppg_scan.items():
                if "pulse_TDCGate/time offset (ms)" in scan:
                    print("TDC trigger delay is being scanned.  TOF is accurate relative to the start the TDC pulse at each step.")
                elif "pulse_TDCGate/pulse width (ms)" in scan:
                    print("TDC gate width is being X scanned.  Using largest value for TOF histogram window.  TOF will still be accurate relative to the start of the TDC pulse.")
                    self.config.tdc_gate_secs = self.get_max_ppg_scan_val(self.bor_odb, scan, "pulse_TDCGate/pulse width (ms)", dim)

        print("Using data from %s TDC, with a max gate of %sms" % (self.config.TDCChoice, self.config.tdc_gate_secs*1000.))

    def get_sane_eva_config(self, odb, midas_xyz):
        cfg = self.get_raw_eva_config(odb, midas_xyz)

        if len(cfg.values) > self.max_eva_scan_length:
            # Truncate to max number of scan points that EVA can handle
            cfg.values = cfg.values[:self.max_eva_scan_length]
            cfg.stop = cfg.values[-1]
        
        return cfg

    def get_raw_eva_config(self, odb, midas_xyz):
        if midas_xyz is None:
            return EVAConfig()

        if midas_xyz == "F":
            retval = EVAConfig()
            retval.dev = self.config.AFG_scan_dev
            retval.fct = 'SetFrequency'
            retval.start = self.config.freq_list[0]
            retval.stop = self.config.freq_list[-1]
            if self.config.num_freq_steps[self.config.AFG_scan_dev] > 0:
                retval.step = (self.config.freq_list[-1] - self.config.freq_list[0]) / (self.config.num_freq_steps[self.config.AFG_scan_dev])
            else:
                retval.step = 0.000000
            retval.unit = 'Hz'
            retval.values = [freq for freq in self.config.freq_list]

            return retval

        if midas_xyz == "X":
            ppg_vars = self.config.scannedXvarsPPG
            epics_vars = self.config.scannedXvarsEPICS
            trapv_vars = self.config.scannedXvarsTrapVoltages
            afgv_vars = self.config.scannedXvarsAfgVoltages
            afgf_vars = self.config.scannedXvarsAfgFreqs
            num_steps = self.config.num_x_steps
        elif midas_xyz == "Y":
            ppg_vars = self.config.scannedYvarsPPG
            epics_vars = self.config.scannedYvarsEPICS
            trapv_vars = self.config.scannedYvarsTrapVoltages
            afgv_vars = self.config.scannedYvarsAfgVoltages
            afgf_vars = self.config.scannedYvarsAfgFreqs
            num_steps = self.config.num_y_steps
        elif midas_xyz == "Z":
            ppg_vars = self.config.scannedZvarsPPG
            epics_vars = self.config.scannedZvarsEPICS
            trapv_vars = self.config.scannedZvarsTrapVoltages
            afgv_vars = self.config.scannedZvarsAfgVoltages
            afgf_vars = self.config.scannedZvarsAfgFreqs
            num_steps = self.config.num_z_steps

        has_ppg = len(ppg_vars) > 0
        has_epics = len(epics_vars) > 0
        has_trapv = len(trapv_vars) > 0
        has_afgv = len(afgv_vars) > 0
        has_afgf = len(afgf_vars) > 0

        fct = "?"
        dev = "?"
        start = 0
        stop = 0
        step = 1
        unit = "?"

        is_combo = False

        if has_ppg and not has_epics and not has_trapv and not has_afgv and not has_afgf:
                dev = "PPG"

                if len(ppg_vars) == 1:
                        fct = ppg_vars[0]
                        start = odb.data["Scanning"]["PPG"]["Settings"]["%s start" % midas_xyz][0]
                        stop = odb.data["Scanning"]["PPG"]["Settings"]["%s end" % midas_xyz][0]
                        step = (stop - start)/num_steps
                        unit = "ms"
                else:
                        is_combo = True
        elif has_epics and not has_ppg and not has_trapv and not has_afgv and not has_afgf:
                dev = "EPICS"

                if len(epics_vars) == 1:
                        fct = epics_vars[0]
                        start = odb.data["Scanning"]["Epics"]["Settings"]["%s start" % midas_xyz][0]
                        stop = odb.data["Scanning"]["Epics"]["Settings"]["%s end" % midas_xyz][0]
                        step = (stop - start)/num_steps
                        unit = "V"
                else:
                        is_combo = True
        elif has_trapv and not has_epics and not has_ppg and not has_afgv and not has_afgf:
                dev = self.config.trapvDevice

                if len(trapv_vars) == 1:
                        fct = trapv_vars[0]
                        start = odb.data["Scanning"][self.config.trapvDevice]["Settings"]["%s start" % midas_xyz][0]
                        stop = odb.data["Scanning"][self.config.trapvDevice]["Settings"]["%s end" % midas_xyz][0]
                        step = (stop - start)/num_steps
                        unit = "V"
                else:
                        is_combo = True
        elif has_afgv and not has_epics and not has_ppg and not has_trapv and not has_afgf:
                dev = "AFG voltage"

                if len(afgv_vars) == 1:
                        fct = afgv_vars[0]
                        dev_dir = fct.replace(" voltage", "")
                        
                        try:
                            start = odb.data["Scanning"][self.config.afg_version]["Voltage"][dev_dir]["Start voltage"]
                            stop = odb.data["Scanning"][self.config.afg_version]["Voltage"][dev_dir]["End voltage"]
                        except KeyError:
                            start = odb.data["Scanning"][self.config.afg_version]["Settings"][dev_dir]["Start voltage"]
                            stop = odb.data["Scanning"][self.config.afg_version]["Settings"][dev_dir]["End voltage"]
                        step = (stop - start)/num_steps
                        unit = "V"
                else:
                        is_combo = True
        elif has_afgf and not has_afgv and not has_epics and not has_ppg and not has_trapv:
                dev = afgf_vars[0]

                if len(afgf_vars) == 1:
                        fct = afgf_vars[0]
                        dev_dir = fct.replace(" frequency", "")
                        
                        start = self.config.freq_list[0]
                        stop = self.config.freq_list[-1]
                        step = (stop - start)/num_steps
                        unit = "Hz"
                else:
                        is_combo = True
        elif not has_ppg and not has_epics and not has_trapv and not has_afgv and not has_afgf:
                dev = "NoScan"
                fct = "NoScan"
        else:
                dev = "Combo"
                is_combo = True

        if is_combo:
            fct = "Combo"
            start = 0
            stop = num_steps
            step = 1
            unit = "Step#"

        vals = list(numpy.linspace(start,stop,num_steps+1))

        retval = EVAConfig()
        retval.dev = dev
        retval.fct = fct
        retval.start = start
        retval.stop = stop
        retval.step = step
        retval.unit = unit
        retval.values = vals
        return retval

    def get_max_ppg_scan_val(self, odb, scannedvarsPPG, var_name, midas_xyz):
        pulseindex = scannedvarsPPG.index(var_name)
        start_val = odb.data["Scanning"]["PPG"]["Settings"]["%s start" % midas_xyz][pulseindex]
        stop_val = odb.data["Scanning"]["PPG"]["Settings"]["%s end" % midas_xyz][pulseindex]
        return max(start_val, stop_val)/1000

    def collect_and_bin_data(self):
        """
        Loop through all events, and put ASUM timestamps into TDC bins.
        
        Fills `self.data`.
        """
        self.mpet_file.midas_file.jump_to_start()
        
        bin_width_s = self.config.tdc_gate_secs / self.config.n_tdc_bins

        first_event_time = None
        self.final_event_ts = 0

        mpet_events = self.mpet_file.get_all_event_info()
        self.num_full_loops = 0

        for mpet_event in mpet_events:
            x = max(0, mpet_event.x_step)
            y = max(0, mpet_event.y_step)
            z = max(0, mpet_event.z_step)
            rep = mpet_event.rep

            combine_reps = False
            if combine_reps:
                loop = mpet_event.loop_count
            else:
                loop = mpet_event.loop_count * self.config.num_reps_at_step + rep

            if self.config.AFG_scan_type == FreqScan.XScan:
                x = 0
            if self.config.AFG_scan_type == FreqScan.YScan:
                y = 0
            if self.config.AFG_scan_type == FreqScan.ZScan:
                z = 0

            evtTime = mpet_event.event_time
            shotnum = -1
            last_count = -1

            if mpet_event.serial_number == 0:
                first_event_time = evtTime

            # binned_data => [per-loop][shot-in-step][TS,{bin:count}]

            while len(self.data[x][y][z].binned_data) <= loop:
                self.data[x][y][z].binned_data.append([])

            if self.config.TDCChoice == "VT2" or self.config.TDCChoice == "VT4":
                if self.config.TDCChoice == "VT2":
                    #asum_chan = 0x1
                    asum_chan = 0x0
                    tdc_list = mpet_event.vt2_data
                if self.config.TDCChoice == "VT4":
                    asum_chan = 0x0
                    tdc_list = mpet_event.tdc_data

                for tdc in tdc_list:

                    if first_event_time is not None:
                        TS = first_event_time + tdc.time_secs
                    else:
                        TS = self.config.run_start_time + tdc.time_secs
                        print("Could not get time stamp from 0th event, using run start time for reference instead (less accurate)")

                    #if there is no AFG scan, combine all data from each event into one "shot"
                    if self.config.AFG_scan_type == FreqScan.NoScan:
                        if len(self.data[x][y][z].binned_data[loop]) == 0:
                            self.data[x][y][z].binned_data[loop].append({"TS": TS, "hist": {}})
                        if tdc.event_type == titan_data.mpet.TDCEventType.timestamp:
                            if asum_chan in tdc.channel_ids:
                                bin = int(math.floor(tdc.tof_secs / bin_width_s))
                                if bin >= 0 and bin < self.config.n_tdc_bins:
                                    if bin in self.data[x][y][z].binned_data[loop][0]["hist"]:
                                        self.data[x][y][z].binned_data[loop][0]["hist"][bin] +=1
                                    else:
                                        self.data[x][y][z].binned_data[loop][0]["hist"][bin] = 1

                    if self.config.AFG_scan_type != FreqScan.NoScan:
                        if self.config.AFG_scan_type == FreqScan.InPPGLoop:
                            if tdc.event_type != titan_data.mpet.TDCEventType.new_cycle:
                                if tdc.count != last_count:
                                    shotnum += 1
                                    last_count = tdc.count
                        elif self.config.AFG_scan_type == FreqScan.XScan:
                            shotnum = mpet_event.x_step
                        elif self.config.AFG_scan_type == FreqScan.YScan:
                            shotnum = mpet_event.y_step
                        elif self.config.AFG_scan_type == FreqScan.ZScan:
                            shotnum = mpet_event.z_step

                        while len(self.data[x][y][z].binned_data[loop]) <= shotnum:
                            self.data[x][y][z].binned_data[loop].append({"TS": TS, "hist": {}})

                        if tdc.event_type == titan_data.mpet.TDCEventType.timestamp:
                            if asum_chan in tdc.channel_ids:
                                bin = int(math.floor(tdc.tof_secs / bin_width_s))
                                if bin >= 0 and bin < self.config.n_tdc_bins:
                                    if bin in self.data[x][y][z].binned_data[loop][shotnum]["hist"]:
                                        self.data[x][y][z].binned_data[loop][shotnum]["hist"][bin] += 1
                                    else:
                                        self.data[x][y][z].binned_data[loop][shotnum]["hist"][bin] = 1
                            


            if self.config.TDCChoice == "V1290":
                fastchan = 1

                #if there is no AFG scan, combine all data from each event into one "shot"
                if self.config.AFG_scan_type == FreqScan.NoScan:
                    TS = evtTime
                    self.data[x][y][z].binned_data[loop].append({"TS": TS, "hist": {}})

                    shotnum = None
                    if self.config.AFG_scan_type == FreqScan.XScan:
                        shotnum = mpet_event.x_step
                    elif self.config.AFG_scan_type == FreqScan.YScan:
                        shotnum = mpet_event.y_step
                    elif self.config.AFG_scan_type == FreqScan.ZScan:
                        shotnum = mpet_event.z_step

                    if shotnum is None and self.config.AFG_scan_type != FreqScan.InPPGLoop:
                        shotnum = 0

                    if shotnum is not None:
                        TS = int(evtTime)

                        while len(self.data[x][y][z].binned_data[loop]) <= shotnum:
                            self.data[x][y][z].binned_data[loop].append({"TS": TS, "hist": {}})

                    for tdc in mpet_event.caen_tdc_parsed:
                        if self.config.AFG_scan_type == FreqScan.InPPGLoop:
                            shotnum = tdc.trigger_count - (mpet_event.serial_number)*self.config.num_freq_steps[self.config.AFG_scan_dev]

                            while len(self.data[x][y][z].binned_data[loop]) <= shotnum:
                                TS = int(evtTime)
                                self.data[x][y][z].binned_data[loop].append({"TS": TS, "hist": {}})
                    
                        if fastchan in tdc.all_tof_secs.keys():
                            for tof in tdc.all_tof_secs[fastchan]:
                                bin = int(math.floor(tof / bin_width_s))
                                if bin >=0 and bin < self.config.n_tdc_bins:
                                    if bin in self.data[x][y][z].binned_data[loop][shotnum]["hist"]:
                                        self.data[x][y][z].binned_data[loop][shotnum]["hist"][bin] +=1
                                    else:
                                        self.data[x][y][z].binned_data[loop][shotnum]["hist"][bin] = 1

                if self.config.AFG_scan_type != FreqScan.NoScan:
                    
                    if mpet_event.serial_number + 1 < len(mpet_events):
                        dt = (mpet_events[mpet_event.serial_number + 1].event_time - evtTime)/self.config.num_freq_steps[self.config.AFG_scan_dev]
                    elif mpet_event.serial_number + 1 == len(mpet_events):
                        dt = (evtTime - mpet_events[mpet_event.serial_number].event_time)/self.config.num_freq_steps[self.config.AFG_scan_dev]
                    else:
                        print("Event serial number is beyond the number of events")

                    shotnum = None
                    if self.config.AFG_scan_type == FreqScan.XScan:
                        shotnum = mpet_event.x_step
                    elif self.config.AFG_scan_type == FreqScan.YScan:
                        shotnum = mpet_event.y_step
                    elif self.config.AFG_scan_type == FreqScan.ZScan:
                        shotnum = mpet_event.z_step

                    if shotnum is not None:
                        TS = int(evtTime + dt*shotnum)

                        while len(self.data[x][y][z].binned_data[loop]) <= shotnum:
                            self.data[x][y][z].binned_data[loop].append({"TS": TS, "hist": {}})

                    for tdc in mpet_event.caen_tdc_parsed:
                        if self.config.AFG_scan_type == FreqScan.InPPGLoop:
                            shotnum = tdc.trigger_count - (mpet_event.serial_number)*self.config.num_freq_steps[self.config.AFG_scan_dev]

                            while len(self.data[x][y][z].binned_data[loop]) <= shotnum:
                                TS = int(evtTime + dt*shotnum)
                                self.data[x][y][z].binned_data[loop].append({"TS": TS, "hist": {}})
                        
                        if fastchan in tdc.all_tof_secs.keys():
                            for tof in tdc.all_tof_secs[fastchan]:
                                bin = int(math.floor(tof / bin_width_s))
                                if bin >= 0 and bin < self.config.n_tdc_bins:
                                    if bin in self.data[x][y][z].binned_data[loop][shotnum]["hist"]:
                                        self.data[x][y][z].binned_data[loop][shotnum]["hist"][bin] += 1
                                    else:
                                        self.data[x][y][z].binned_data[loop][shotnum]["hist"][bin] = 1

        """
        Count the number of full loops
        """
        self.num_full_loops = 0
        nx = self.config.num_x_steps
        ny = self.config.num_y_steps
        nz = self.config.num_z_steps

        num_total_loops = len(self.data[0][0][0].binned_data)

        if nx in self.data and ny in self.data[nx] and nz in self.data[nx][ny]:
            for loop in range(num_total_loops):
                try:
                    if len(self.data[nx][ny][nz].binned_data[loop]) >= self.config.num_freq_steps[self.config.AFG_scan_dev]:
                        self.num_full_loops = loop + 1
                except IndexError:
                    pass
        else:
            self.num_full_loops = 0

        if self.num_full_loops < num_total_loops:
            if self.only_write_full_loops:
                print("\nONLY USING DATA FROM THE FIRST %d FULL SCAN LOOPS!" % (self.num_full_loops / self.config.num_reps_at_step))
            else:
                print("\nUsing all data in the midas file, including the final incomplete loop.")
        else:
            print("\nUsing all data in the midas file.")

        """
        Check to make sure the number of shots in binned_data is equal to the number of frquency steps.
        If it's the last event, then it's ok for there to be fewer shots because the run might have been stopped before the end of a ppg cycle, but there still should never be more shots.
        
        Also check for the max event timestamp while we're looping through the data.
        """
        self.final_event_ts = 0

        if self.config.AFG_scan_type != FreqScan.NoScan:
            for x in self.data.keys():
                for y in self.data[x].keys():
                    for z in self.data[x][y].keys():
                        for loop in range(self.num_full_loops):
                            if len(self.data[x][y][z].binned_data[loop]) != self.config.num_freq_steps[self.config.AFG_scan_dev]:
                                if loop == len(self.data[x][y][z].binned_data) - 1:
                                    if len(self.data[x][y][z].binned_data[loop]) > self.config.num_freq_steps[self.config.AFG_scan_dev]:
                                        print("Something is off, there are %d shots in x=%d,y=%d,z=%d,l=%d but %d frequency points." % (len(self.data[x][y][z].binned_data[loop]), x, y, z, loop, self.config.num_freq_steps[self.config.AFG_scan_dev]))
                                else:
                                        print("Something is off, there are %d shots in x=%d,y=%d,z=%d,l=%d but %d frequency points." % (len(self.data[x][y][z].binned_data[loop]), x, y, z, loop, self.config.num_freq_steps[self.config.AFG_scan_dev])) 

                            if len(self.data[x][y][z].binned_data[loop]) > 0:
                                last_shot = self.data[x][y][z].binned_data[loop][-1]
                                if last_shot["TS"] > self.final_event_ts:
                                    self.final_event_ts = last_shot["TS"]

        
    def get_output_filename(self, suffix):
        """
        Get the filename we should use. This is based on the input
        midas file name (`self.input_file_path`), with customisations.
        
        Args:
            
        * suffix (str) - Custom suffix and extension, which will replace the
            .mid of the midas file. E.g. "_eva.dat".

        
        Returns:
            str (e.g. "run00011_eva.dat")
        """
        midfile = os.path.basename(self.input_file_path)
        (midname, ext) = os.path.splitext(midfile)
        
        if midname.endswith(".mid"):
            # Midas file was compressed, and so far we only removed .lz4, .gz etc.
            midname = midname[:-4]
        
        return midname + suffix
    
    def get_default_output_dir(self, filetype):
        """
        Get the default directory we should write output files to.
        Based on main data directory, file type, and date of run.
        
        Args:
            
        * filetype (str) - E.g. "eva"
        
        Returns:
            str
        """
        dt = datetime.datetime.fromtimestamp(self.config.run_start_time)
        subdir = dt.strftime("%Y/%Y%m%d")
        return "/titan/data5/mpet/%s/%s" % (filetype, subdir)
        
    def add_shot_to_file(self, datafile, x, y, z, loop, shot_idx):
        if loop < len(self.data[x][y][z].binned_data) and shot_idx < len(self.data[x][y][z].binned_data[loop]):
            TS = self.data[x][y][z].binned_data[loop][shot_idx]["TS"]
            hist = self.data[x][y][z].binned_data[loop][shot_idx]["hist"]
        else:
            # Empty event to pad out incomplete loops (to satisfy EVA data format,
            # expecially in cases where we're writing data in a different order than
            # it was acquired in).
            # Use timestamp of last event in file so EVA doesn't declare the
            # "end of run" as UNIX time 0.
            TS = self.final_event_ts
            hist = {}

        datafile.write(struct.pack('h', len(hist)*4 + 4))
        datafile.write(struct.pack('i', int(TS)))

        for bin in sorted(hist.keys()):
            datafile.write(struct.pack('h', bin))
            datafile.write(struct.pack('h', hist[bin]))

    def write_eva_file(self, outdir=None):
        """
        Write an Eva file to disk, based on the content of `self.data`.
        
        Args:
            
        * outdir (str or None) - Directory to write to. If not set, will
            be computed by `get_default_output_dir()`.

        Returns:
            list of string - Filenames written
            
        This curently assumes that all scans were done in "lowest-to-highest"
        mode, with no randomisation of scan values.
        """

        f_scan = self.config.AFG_scan_type != FreqScan.NoScan
        x_scan = self.config.scan_settings["Enable X"] and self.config.AFG_scan_type != FreqScan.XScan
        y_scan = self.config.scan_settings["Enable Y"] and self.config.AFG_scan_type != FreqScan.YScan
        z_scan = self.config.scan_settings["Enable Z"] and self.config.AFG_scan_type != FreqScan.ZScan

        combos = []
        files_written = []

        if f_scan:
            if x_scan:
                combos.append(["F", "X"])
                combos.append(["X", "F"])
            if y_scan:
                combos.append(["F", "Y"])
                combos.append(["Y", "F"])
            if z_scan:
                combos.append(["F", "Z"])
                combos.append(["Z", "F"])
            if not x_scan and not y_scan and not z_scan:
                combos.append(["F", None])
        if x_scan:
            if y_scan:
                combos.append(["X", "Y"])
                combos.append(["Y", "X"])
            if z_scan:
                combos.append(["X", "Z"])
                combos.append(["Z", "X"])
            if not f_scan and not y_scan and not z_scan:
                combos.append(["X", None])
        if y_scan:
            if z_scan:
                combos.append(["Y", "Z"])
                combos.append(["Z", "Y"])
            if not f_scan and not x_scan and not z_scan:
                combos.append(["Y", None])
        if z_scan:
            if not f_scan and not x_scan and not y_scan:
                combos.append(["Z", None])
        if not f_scan and not x_scan and not y_scan and not f_scan:
            combos.append([None, None])

        if outdir is None:
            outdir = self.get_default_output_dir("evafiles")

        if not os.path.exists(outdir):
            os.makedirs(outdir)

        for combo in combos:
            eva0 = self.get_sane_eva_config(self.bor_odb, combo[0])
            eva1 = self.get_sane_eva_config(self.bor_odb, combo[1])

            combo_human = [c if c is None else c.replace("F", "Freq") for c in combo]
            print("")

            if combo[0] is None and combo[1] is None:
                print("Writing a single EVA file (nothing was scanned)")
                suffix = "_eva.dat"
            elif combo[1] is None:
                print("Writing EVA file with %s scan as main scan dimension" % combo_human[0])
                suffix = "_%s_eva.dat" % combo_human[0]
            else:
                print("Writing EVA file with %s scan as main scan dimension and %s scan as second dimension" % (combo_human[0], combo_human[1]))
                suffix = "_%s_%s_eva.dat" % (combo_human[0], combo_human[1])

            evafilename = self.get_output_filename(suffix)
            evafilepath = os.path.join(outdir, evafilename)
            datafile = open(evafilepath, 'wb')
                    
            # this will be the header length
            datafile.write(struct.pack('i', 1))
            # this will be the start of the binary TOF data
            datafile.write(struct.pack('i', 2))

            # Write the header info
            datafile.write(b'\n\n[Mass]\n Mass=%s,Charge=%s\n\n' % (self.config.species.encode('utf-8'), self.config.charge.encode('utf-8')))
            datafile.write(b'[Switch]\n NrCycles=-1\n\n')
            datafile.write(b'[Excit]\n Mass=%s ,Charge= %s,' % (self.config.species.encode('utf-8'), self.config.charge.encode('utf-8')))
            datafile.write(b'Freq=%f, Amp= %f,' % (self.config.centre_freq, self.config.rf_amplitude))
            datafile.write(b'Time=%f\n\n' % self.config.rf_time_secs)
            datafile.write(b'[MCA]\n MCA=%s,TimePerChannel=%f' % (self.config.TDCChoice.encode('utf-8'), (self.config.tdc_gate_secs * 1e6 / self.config.n_tdc_bins)))
            datafile.write(b'\xB5' + b's,Channels= %d' % self.config.n_tdc_bins)
            datafile.write(b',Pipse=   0\n\n')
            datafile.write(b'[SCAN0]\n Dev=%s, Fct=%s, Spec=,\n' % (eva0.dev.encode('utf-8'), eva0.fct.encode('utf-8')))
            datafile.write(b' Start=%f, Stop=%f, Step=%f, Unit=%s\n\n' % (eva0.start, eva0.stop, eva0.step, eva0.unit.encode('utf-8')))
            datafile.write(b'[SCAN1]\n Dev=%s, Fct=%s, Spec=,\n' % (eva1.dev.encode('utf-8'), eva1.fct.encode('utf-8')))
            datafile.write(b' Start=%f, Stop=%f, Step=%f, Unit=%s\n\n' % (eva1.start, eva1.stop, eva1.step, eva1.unit.encode('utf-8')))
            datafile.write(b'*---------------here the binary part starts---------------*\n')
            headerlen = int(datafile.tell()) - 8
            datafile.seek(0)
            datafile.write(struct.pack('i', headerlen))
            datafile.seek(0, 2)

            # write the scanned values
            datafile.write(struct.pack('i', len(eva0.values)))
            for val in eva0.values:
                datafile.write(struct.pack('d', val))
    
            if len(eva1.values) > 0:
                datafile.write(struct.pack('i', len(eva1.values)))
                for val in eva1.values:
                    datafile.write(struct.pack('d', val))
            else:
                datafile.write(struct.pack('i', 1))
                datafile.write(struct.pack('d', 0.0))

            # get and write the location of the binary data start
            datastart = int(datafile.tell())
            datafile.seek(4)
            datafile.write(struct.pack('i', datastart))
            datafile.seek(datastart)

            if self.only_write_full_loops:
                num_loops = self.num_full_loops
            else:
                num_loops = len(self.data[0][0][0].binned_data)

            num_freqs = min(self.max_eva_scan_length, len(self.data[0][0][0].binned_data[0]))
            num_x = min(self.max_eva_scan_length, self.config.num_x_steps + 1)
            num_y = min(self.max_eva_scan_length, self.config.num_y_steps + 1)
            num_z = min(self.max_eva_scan_length, self.config.num_z_steps + 1)

            if combo[0] == "F" and len(self.data[0][0][0].binned_data[0]) > self.max_eva_scan_length:
                print("> Limiting this file to the first %d frequencies, as EVA can't handle more than that!" % self.max_eva_scan_length)
            if combo[0] == "X" and self.config.num_x_steps + 1 > self.max_eva_scan_length:
                print("> Limiting this file to the first %d X steps, as EVA can't handle more than that!" % self.max_eva_scan_length)
            if combo[0] == "Y" and self.config.num_y_steps + 1 > self.max_eva_scan_length:
                print("> Limiting this file to the first %d Y steps, as EVA can't handle more than that!" % self.max_eva_scan_length)
            if combo[0] == "Z" and self.config.num_z_steps + 1 > self.max_eva_scan_length:
                print("> Limiting this file to the first %d Z steps, as EVA can't handle more than that!" % self.max_eva_scan_length)

            # Inner-most loop is SCAN0; then SCAN1; then others
            for loop in range(num_loops):
                if combo == ["F", "Z"] or combo == ["F", None] or combo == [None, None]:
                    for x in range(num_x):
                        for y in range(num_y):
                            for z in range(num_z):
                                for shot_idx in range(num_freqs):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
                if combo == ["F", "X"]:
                    for y in range(num_y):
                        for z in range(num_z):
                            for x in range(num_x):
                                for shot_idx in range(num_freqs):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
                if combo == ["F", "Y"]:
                    for x in range(num_x):
                        for z in range(num_z):
                            for y in range(num_y):
                                for shot_idx in range(num_freqs):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
                if combo == ["X", "Y"] or combo == ["X", None]:
                    for z in range(num_z):
                        for shot_idx in range(num_freqs):
                            for y in range(num_y):
                                for x in range(num_x):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
                if combo == ["Y", "X"] or combo == ["Y", None]:
                    for z in range(num_z):
                        for shot_idx in range(num_freqs):
                            for x in range(num_x):
                                for y in range(num_y):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
                if combo == ["X", "F"]:
                    for z in range(num_z):
                        for y in range(num_y):
                            for shot_idx in range(num_freqs):
                                for x in range(num_x):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
                if combo == ["X", "Z"]:
                    for y in range(num_y):
                        for shot_idx in range(num_freqs):
                            for z in range(num_z):
                                for x in range(num_x):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
                if combo == ["Z", "X"] or combo == ["Z", None]:
                    for y in range(num_y):
                        for shot_idx in range(num_freqs):
                            for x in range(num_x):
                                for z in range(num_z):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
                if combo == ["Y", "F"]:
                    for x in range(num_x):
                        for z in range(num_z):
                            for shot_idx in range(num_freqs):
                                for y in range(num_y):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
                if combo == ["Y", "Z"]:
                    for x in range(num_x):
                        for shot_idx in range(num_freqs):
                            for z in range(num_z):
                                for y in range(num_y):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
                if combo == ["Z", "Y"]:
                    for x in range(num_x):
                        for shot_idx in range(num_freqs):
                            for y in range(num_y):
                                for z in range(num_z):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
                if combo == ["Z", "F"]:
                    for x in range(num_x):
                        for y in range(num_y):
                            for shot_idx in range(num_freqs):
                                for z in range(num_z):
                                    self.add_shot_to_file(datafile, x, y, z, loop, shot_idx)
            
            datafile.close()
            print("> Wrote %s" % evafilepath)

            files_written.append(evafilepath)

        return files_written

            

def get_arg_parser():
    parser = argparse.ArgumentParser(description="Convert MPET midas file to eva format")
    parser.add_argument("-e", "--ElementSymbol", dest="elem",
                        help="Chemical denomination, i.e. 1Li6, or 1C12.")
    parser.add_argument("-z", "--Charge", dest="z", type=float,
                        help="Ion charge.")
    parser.add_argument("-i", "--BinNumber", dest="nbins", type=int,
                        help="Number of TDC bins.")
    parser.add_argument("-g", "--TDCGateSecs", dest="tdc_gate_secs", type=float,
                        help="Max ToF for EVA histograms, in seconds.")
    parser.add_argument("-t", "--TDCChoice", dest="tdc",
                        help="Specify V1290, VT4, or VT2 TDC.")
    parser.add_argument("-a", "--AFGChoice", dest="afg",
                        help="Choose to display dipole or quad afg parameters if both were scanned.")
    parser.add_argument("-d", "--OutputDir", dest="outdir",
                        default="/triumfcs/trshare/titan/MPET/Data/",
                        help="Output directory for Eva file")
    parser.add_argument("--only-full-loops", action="store_true", 
                        help="Only write data from full scan loops, not the final incomplete loop.")
    parser.add_argument("midas_file", help="Input midas file")

    return parser
            
def run(args):
    """
    Main function for converting a midas file to Eva format.
    """
    if not os.path.exists(args.midas_file):
        raise ValueError("Input midas file %s does not exist" % args.midas_file)
      
    if not os.path.exists(args.outdir):
        raise ValueError("Output directory %s does not exist" % args.outdir)
      
    if not os.path.isdir(args.outdir):
        raise ValueError("Output path %s is not a directory" % args.outdir)

    m2e = MidasToEva(args.midas_file)
    m2e.populate_config(args)
    m2e.collect_and_bin_data()
    m2e.write_eva_file(args.outdir)

if __name__ == "__main__":
    parser = get_arg_parser()
    args = parser.parse_args()
    run(args)
