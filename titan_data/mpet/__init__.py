"""
Tools for parsing MPET data.

This tool is based on the midas python file reader, which is found in
$MIDASSYS/python/file_reader.py, and was added to midas in late 2019.


Example usage for iterating over events in python:

```
import titan_data.mpet

mpet_file = titan_data.mpet.MPETFile("/path/to/midas_file.mid")

for mpet_event in mpet_file:
    print("Accumulation time for this event is %s ms" % mpet_event.accumulation_time_ms)
    print("Event has %s timestamps and %s positions" % (len(mpet_event.tdc_data), len(mpet_event.pos_data)))
```



Example usage for grabbing all events as a list:

```
import titan_data.mpet

mpet_file = titan_data.mpet.MPETFile("/path/to/midas_file.mid")
mpet_events = mpet_file.get_all_event_info()

for mpet_event in mpet_events:
    print("Accumulation time for this event is %s ms" % mpet_event.accumulation_time_ms)
    print("Event has %s timestamps and %s positions" % (len(mpet_event.tdc_data), len(mpet_event.pos_data)))
```

"""

import titan_data.midas.file_reader as file_reader
import datetime

class MPETConstants:
    def __init__(self):
        # PPG programming block for extracting the accumulation time
        self.acc_time_block_name = "pulse_QUAD1"
        self.acc_time_odb_path = "/Equipment/PPGCompiler/Programming/%s/pulse width (ms)" % self.acc_time_block_name
        
        # Special V1290 channels for Roentdek DLD MCP
        self.v1290_chan_mcp_fast = 1
        self.v1290_chan_mcp_x1 = 2
        self.v1290_chan_mcp_x2 = 3
        self.v1290_chan_mcp_y1 = 4
        self.v1290_chan_mcp_y2 = 5
        
        # Name of the pulse that triggers the CAEN V1290 digitizer.
        # We use this to automatically look up an offset to apply to
        # ToF calculations from the V1290. E.g. if the start of the
        # CAEN trigger window is 10us after the start of the pulse that 
        # pulse_caen_end_trig_window is defined relative to, we'll add
        # 10us to all the ToF calculations.
        #
        # Setting this param to None will disable this extra calculation
        # and give you the ToF relative to the start of the CAEN trigger
        # window only.
        self.v1290_t0_offset_from_ppg_block = ["pulse_caen_end_trig_window", "multi_caen_trig"]
        
        # 40MHz clock stepped down by 2^10 => ~24.41ps per bin
        self.v1290_bin_width_ps = 1e6 / (40 * 2**10)

        # 25ns bins for trigger time tag
        self.v1290_ettt_lsb_ns = 25
        
        # Constants for delay time to position for Roentdek DLD MCP
        self.dld40_ns_per_mm_x = 1.5
        self.dld40_ns_per_mm_y = 1.5
        self.dld40_offset_mm_x = 0
        self.dld40_offset_mm_y = 0

        # Constants for converting Quantar/LRS position data (0-256) to mm
        self.quantar_mcp_diameter_mm = 28
        self.quantar_mcp_center_counts_x = 128
        self.quantar_mcp_center_counts_y = 128
        
        # Compute an event's "trap time" as the time difference between these two pulses
        # in the PPG program.
        self.trap_time_in_from_ppg_block = "pulse_trapv_injection"
        self.trap_time_ej_from_ppg_block = "pulse_trapv_ejection"

class MPETFile:
    """
    Class that provides easy access to MPET-specific data 
    """
    def __init__(self, file_path):
        self.midas_file = file_reader.MidasFile(file_path)
        eor = self.midas_file.get_eor_odb_dump().data
        bor = self.midas_file.get_bor_odb_dump().data
        self.run_info = get_mpet_run_info(bor, eor)
        
    def get_all_event_info(self):
        """
        Returns data for all events in a file as a list.
        This may be slow and memory-inefficient if you have a large number
        of events in the file.
        """
        return [ev for ev in self]

    def __next__(self):
        """
        Iterable interface for looping through events.
        """
        while True:
            midas_event = self.midas_file.read_next_event()
            
            if midas_event is None:
                # End of file - stop iterating
                raise StopIteration()
            
            # Try to convert to an MPETEvent
            try:
                mpet_event = get_mpet_event_info(midas_event, self.run_info)
            except TypeError:
                print ("TypeError while getting mpet event info - possible problem with midas event?")
                continue
                        
            if mpet_event is None:
                # Internal midas event; move on to next real event
                continue
                
            return mpet_event
    
    next = __next__ # for Python 2        
        
    def __iter__(self):
        """
        Iterable interface for looping through events.
        """
        return self

class MPETRunInfo:
    def __init__(self):
        """
        Overall configuration of voltage/EPICS scanning etc.
        
        If scanning is disabled, most of the lists will be empty.
        
        Members:
            
        * run_number (int) - Midas run number
        * run_start_time (int) - UNIX timestamp when run started
        * num_x_steps (int) - Number of X steps in scan (data is taken at N+1 points)
        * num_y_steps (int) - Number of Y steps in scan (data is taken at N+1 points)
        * num_z_steps (int) - Number of Y steps in scan (data is taken at N+1 points)
        * num_reps_at_step (int) - Number of times to repeat each X/Y config before moving to next step
        * scan_ppg_x (list of str) - list of ODB paths changed in each X step
        * scan_ppg_y (list of str) - list of ODB paths changed in each Y step
        * scan_ppg_z (list of str) - list of ODB paths changed in each Z step
        * scan_epics_x (list of str) - list of devices changed in each X step
        * scan_epics_y (list of str) - list of devices changed in each Y step
        * scan_epics_z (list of str) - list of devices changed in each Z step
        * scan_trapv_x (list of 2-tuples of (str, int)) - list of block/channel changed in each X step (e.g. 'injection', 3)
        * scan_trapv_y (list of 2-tuples of (str, int)) - list of block/channel changed in each Y step (e.g. 'injection', 3)
        * scan_trapv_z (list of 2-tuples of (str, int)) - list of block/channel changed in each Z step (e.g. 'injection', 3)
        * scan_afg_volt_x (list of str) - list of AFG device voltages changed in each X step
        * scan_afg_volt_y (list of str) - list of AFG device voltages changed in each Y step
        * scan_afg_volt_z (list of str) - list of AFG device voltages changed in each Z step
        * scan_afg_freq_x (list of str) - list of AFG device frequencies changed in each X step
        * scan_afg_freq_y (list of str) - list of AFG device frequencies changed in each Y step
        * scan_afg_freq_z (list of str) - list of AFG device frequencies changed in each Z step
        * freq_scanned_in_ppg (bool) - Whether frequency scanned in PPG loop (Agilent AFG) or as regular variable (Tektronix AFG)
        * quad_freqs (list of float) - Quad frequencies applied
        * dipole_freqs (list of float) - Dipole frequencies applied
        * caen_trig_window_width_ms (float) - width of the V1290 trigger window, in seconds
        * caen_trig_window_offset_ms (float) - offset of the V1290 trigger window, in seconds (signed!)
        * caen_rel_to_pulse_name (str) - PPG pulse that the CAEN trigger pulse was defined relative to in the PPG program
        * ppg_program (dict) - PPG program being executed, as recorded in the ODB
        * ppg_t0_offset_order (list of str) - Order of PPG offsets from T0 in the TPPG bank if scanning PPG
        * default_trap_time_ms (float) - Time between injection and ejection, if not scanning
        * default_caen_t0_offset_ms (float) - See `MPETEvent.caen_t0_offset_ms`. This is the default value if not scanning.
        * trapv_channel_names (list of str) - human-readable name of each trap voltage channel
        """
        self.run_number = None
        self.run_start_time = None
        self.num_x_steps = 0
        self.num_y_steps = 0
        self.num_z_steps = 0
        self.num_reps_at_step = 0
        self.scan_ppg_x = []
        self.scan_ppg_y = []
        self.scan_ppg_z = []
        self.scan_epics_x = []
        self.scan_epics_y = []
        self.scan_epics_z = []
        self.scan_trapv_x = []
        self.scan_trapv_y = []
        self.scan_trapv_z = []
        self.scan_afg_volt_x = []
        self.scan_afg_volt_y = []
        self.scan_afg_volt_z = []
        self.scan_afg_freq_x = []
        self.scan_afg_freq_y = []
        self.scan_afg_freq_z = []
        self.freq_scanned_in_ppg = True
        self.quad_freqs = []
        self.dipole_freqs = []
        self.caen_trig_window_width_ms = 0
        self.caen_trig_window_offset_ms = 0
        self.caen_trig_window_offset_ms = 0
        self.caen_rel_to_pulse_name = None
        self.ppg_program = None
        self.ppg_t0_offset_order = []
        self.default_trap_time_ms = None
        self.default_caen_t0_offset_ms = None
        self.trapv_channel_names = []

    def dump_to_screen(self):
        print("Run # %s, started at %s" % (self.run_number, datetime.datetime.fromtimestamp(self.run_start_time)))
        
        if self.num_x_steps > 0 and self.num_y_steps > 0 and self.num_z_steps > 0:
            print("  3D scan over %d X points and %d Y points and %d Z points" % (self.num_x_steps + 1, self.num_y_steps + 1, self.num_z_steps + 1))
        elif self.num_x_steps > 0 and self.num_y_steps > 0:
            print("  2D scan over %d X points and %d Y points" % (self.num_x_steps + 1, self.num_y_steps + 1))
        elif self.num_x_steps > 0:
            print("  1D scan over %d X points" % (self.num_x_steps + 1))
        else:
            print("  No variables being scanned")
            
        print("  Repeating each step %d times before moving on" % self.num_reps_at_step)
        print("  CAEN trigger window info:")
            
        if self.caen_trig_window_width_ms is not None and self.caen_trig_window_offset_ms is not None:
            print("    Window width %.2fus, at an offset of %.2fus" % (self.caen_trig_window_width_ms*1000., self.caen_trig_window_offset_ms*1000.))
        
        if self.caen_rel_to_pulse_name is not None:
            print("    T0 offset is being computed relative to pulse %s" % self.caen_rel_to_pulse_name)
            
        if self.default_caen_t0_offset_ms is not None:
            print("    Default T0 offset is %.2fus" % (self.default_caen_t0_offset_ms * 1000.))
        else:
            print("    Default T0 offset is unknown")

        if self.default_trap_time_ms is not None:
            print("  Default trap time is %.3fms" % (self.default_trap_time_ms))
        else:
            print("  Default trap time is unknown")

    def get_all_scanned_var_names(self):
        x_keys = []
        y_keys = []
        z_keys = []

        if self.num_x_steps > 0:
            x_keys.extend([p for p in self.scan_ppg_x if p != ""])
            x_keys.extend([p for p in self.scan_epics_x if p != ""])
            x_keys.extend([trapv_tuple_to_str(p, self) for p in self.scan_trapv_x if p[0] != ""])
            x_keys.extend(self.scan_afg_volt_x)
            x_keys.extend(self.scan_afg_freq_x)

        if self.num_y_steps > 0:
            y_keys.extend([p for p in self.scan_ppg_y if p != ""])
            y_keys.extend([p for p in self.scan_epics_y if p != ""])
            y_keys.extend([trapv_tuple_to_str(p, self) for p in self.scan_trapv_y if p[0] != ""])
            y_keys.extend(self.scan_afg_volt_y)
            y_keys.extend(self.scan_afg_freq_y)

        if self.num_z_steps > 0:
            z_keys.extend([p for p in self.scan_ppg_z if p != ""])
            z_keys.extend([p for p in self.scan_epics_z if p != ""])
            z_keys.extend([trapv_tuple_to_str(p, self) for p in self.scan_trapv_z if p[0] != ""])
            z_keys.extend(self.scan_afg_volt_z)
            z_keys.extend(self.scan_afg_freq_z)

        return (x_keys, y_keys, z_keys)

class TDCEventType:
    """
    Trivial class to enumerate TDC event types.
    """
    gate_start = 1
    gate_end = 2
    new_cycle = 3
    timestamp = 4
    
class TDC:
    """
    Represent a single TDC "event", e.g. start of a cycle or a timestamp.
    
    Members:
        
    * time_secs (float) - Time of this event
    * tof_secs (float) - Time difference from previous "gate_start" event
    * count (int) - For timestamp/gate_start, the frequency index. For new_cycle,
        the PPG cycle number.
    * channel_ids (list of int) - For timestamp, the channels hit
    * event_type (int) - Member of `TDCEventType`
    * ppg_cycle (int) - PPG cycle number.
    """
    def __init__(self):
        self.time_secs = None
        self.tof_secs = None
        self.count = None
        self.channel_ids = []
        self.event_type = None
        self.ppg_cycle = None

    def __str__(self):
        ev_name = "%04d" % self.event_type if isinstance(self.event_type, int) else "????"
        if self.event_type == TDCEventType.gate_start:
            ev_name = "begg"
        if self.event_type == TDCEventType.gate_end:
            ev_name = "endg"
        if self.event_type == TDCEventType.new_cycle:
            ev_name = "cycl"
        if self.event_type == TDCEventType.timestamp:
            ev_name = "stmp"
            
        tof_str = ""
        if self.tof_secs is not None:
            tof_str = "; ToF %.3fms" % (self.tof_secs*1000.)
        
        return "TDC event type %s at time %fs; count %u; channels %s%s" % (ev_name, self.time_secs, self.count, self.channel_ids, tof_str)

class CaenTDC:
    """
    Represents a single trigger from the CAEN V1290 25ps TDC, with
    MPET-specific conversions to ToF and MCP positions, based on
    the following channel setup:
    * 0 - unconnected
    * 1 - MCP fast signal
    * 2 - MCP delay X1
    * 3 - MCP delay X2
    * 4 - MCP delay Y1
    * 5 - MCP delay Y2
    * Others - just ToF relative to start of trigger window
    
    Members:
      
    * trigger_count (int) - Increments each time V1290 receives trigger signal
    * pos_x_mm (list of float) - MCP X position in mm
    * pos_y_mm (list of float) - MCP Y position in mm
    * mcp_tof_secs (list of float) - ToF from trap ejection to MCP, in seconds
    * all_tof_secs (dict of {int, list of float}) - ToF from trap ejection for all channels, in seconds
    * failed_to_compute_positions (bool) - Set to True if there were different numbers of timestamps on
        the MCP delay channels (so we couldn't easily match them to ion hits).
    """
    def __init__(self):
        self.trigger_count = None
        self.pos_x_mm = []
        self.pos_y_mm = []
        self.mcp_tof_secs = []
        self.all_tof_secs = {}
        self.failed_to_compute_positions = False
        
    def __str__(self):
        tof_str = "MCP ToF (us): %s" % [tof * 1e6 for tof in self.mcp_tof_secs]

        if self.failed_to_compute_positions:
            data_str = "Failed to compute positions!"
        elif len(self.pos_x_mm) == 0:
            data_str = "Parsed 0 hits. Has data on %d channels: %s" % (len(self.all_tof_secs), sorted(self.all_tof_secs.keys()))
        else:
            hit_strs = []
            
            for i in range(len(self.pos_x_mm)):
                hit_strs.append("(%.2fmm, %.2fmm) @ %.3fus" % (self.pos_x_mm[i], self.pos_y_mm[i], (self.mcp_tof_secs[i] * 1e6)))
            
            data_str = "Has %d hits: %s" % (len(hit_strs), "; ".join(hit_strs))
            
        return "CAEN trigger %d. %s. %s" % (self.trigger_count, tof_str, data_str)

class CaenTDCRaw:
    """
    Represents a single timestamp from the CAEN V1290 25ps TDC
    
    Members:
        
    * trigger_count (int) - Increments each time V1290 receives trigger signal
    * channel_id (int) - 0 to 15
    * timestamp_secs (float) - Relative to the start of the trigger window
    * ettt_secs (float) - Extended trigger time tag (if ETTT enabled). Rolls over every 107.4s
    """
    def __init__(self):
        self.ppg_cycle = None
        self.trigger_count = None
        self.channel_id = None
        self.timestamp_secs = None
        self.ettt_secs = None

    def __str__(self):
        ettt_str = "" if self.ettt_secs is None else " (ETTT %fms)" % (self.ettt_secs * 1000)
        return "CAEN TDC hit in trigger ID %d%s; timestamp %fms, channel %d" % (self.trigger_count, ettt_str, self.timestamp_secs * 1000, self.channel_id)
    
class Position:
    """
    Represents a single position hit.
    
    Members:
        
    * ppg_cycle (int) - PPG cycle number
    * x (int) - X position (0-255)
    * y (int) - Y position (0-255)
    * x_mm (float) - X position in mm
    * y_mm (float) - Y position in mm
    """
    def __init__(self, ppg_cycle, x, y, constants):
        """
        Args:

        * ppg_cycle (int) - PPG cycle number
        * x (int) - X position (0-255)
        * y (int) - Y position (0-255)
        * constants (`MPETConstants`)
        """
        self.ppg_cycle = ppg_cycle
        self.x = x
        self.y = y
        self.x_mm = (self.x - constants.quantar_mcp_center_counts_x) / 256. * constants.quantar_mcp_diameter_mm
        self.y_mm = (self.y - constants.quantar_mcp_center_counts_y) / 256. * constants.quantar_mcp_diameter_mm

    def __str__(self):
        return "PPG cycle %s, X %s (%.1fmm), Y %s (%.1fmm)" % (self.ppg_cycle, self.x, self.x_mm, self.y, self.y_mm)

class MPETEvent:
    """
    Stores data from a single PPG cycle.
    
    Members:
    
    * event_time (int) - UNIX timestamp when event was read out
    * serial_number (int) - Midas event number
    * accumulation_time_ms (float) - Time of the pulse_QUAD pulse from the PPG
        program (whether constant or being scanned during a run)
    * trap_time_ms (float) - Time between the injection and ejection pulse in the PPG program
    * loop_count (int) - Overall loop number (number of scan sweeps completed)
    * x_step (int) - X step of the current loop
    * y_step (int) - Y step of the current loop
    * rep (int) - Repetition of the same X/Y config before moving to the next step
    * ppg_scan_values (dict of {str: float}) - For PPG values that are being scanned, ODB path -> value
    * epics_scan_values (dict of {str: float}) - For EPICS values that are being scanned, device name -> value
    * trapv_scan_values (dict of {(str, int): float}) - For softdac/16ao16 values that are being scanned, (block, channel) -> value
    * afg_volt_scan_values (dict of {str: float}) - For AFG devices being scanned, the voltage (keys "Quad" and "Dipole")
    * afg_freq_scan_values (dict of {str: float}) - For AFG devices being scanned, the frequency (keys "Quad" and "Dipole")
    * caen_tdc_raw (list of `CaenTDCRaw`) - Raw data from V1290 (just timestamps)
    * caen_tdc_parsed (list of `CaenTDC`) - Parsed data from V1290 (converted to ToF and/or positions)
    * caen_tdc_error (None or string) - If V1290 reported an error condition, what the error(s) are
    * caen_t0_offset_ms (float) - Time between the start of the CAEN trigger and the PPG pulse that ToF should
        be computed relative to, in ms. This is accounted for in the `caen_tdc_parsed` entries (but not
        the `caen_tdc_raw` ones).
    * tdc_data (list of `TDC`) - From the old MCP and VT4
    * vt2_data (list of `TDC`) - From the old MCP and VT2
    * pos_data (list of `Position`) - From the old MCP, analog position alayzer, and LRS1190
    * original_midas_event (`midas.event.MidasEvent`)
    """
    def __init__(self):
        self.event_time = None
        self.serial_number = None
        self.accumulation_time_ms = None
        self.trap_time_ms = None
        
        self.loop_count = 0
        self.x_step = 0
        self.y_step = 0
        self.z_step = 0
        self.rep = 0
        
        self.ppg_scan_values = {} 
        self.epics_scan_values = {}
        self.trapv_scan_values = {}
        self.afg_volt_scan_values = {}
        self.afg_freq_scan_values = {}
        
        self.caen_tdc_raw = []
        self.caen_tdc_parsed = []
        self.caen_tdc_error = None
        self.caen_t0_offset_ms = None
        self.caen_first_ettt_secs = None
        self.tdc_data = []
        self.vt2_data = []
        self.pos_data = []
        
        self.original_midas_event = None
        
    def dump_to_screen(self):
        print("Event # %s, at %s" % (self.serial_number, datetime.datetime.fromtimestamp(self.event_time)))
        print("  Loop count %d, X point %d, Y point %d, Z point %d, rep %d" % (self.loop_count, self.x_step, self.y_step, self.z_step, self.rep))
        
        if self.accumulation_time_ms is None:
            print("  Accumulation time unknown")
        else:
            print("  Accumulation time %.3fms" % self.accumulation_time_ms)
        
        if self.trap_time_ms is None:   
            print("  Trap time unknown")
        else:
            print("  Trap time %.3fms" % self.trap_time_ms)
        
        if len(self.ppg_scan_values):
            print("  PPG scanning:")
            for k, v in self.ppg_scan_values.items():
                print("    %s => %s" % (k, v))
        if len(self.epics_scan_values):
            print("  EPICS scanning:")
            for k, v in self.epics_scan_values.items():
                print("    %s => %s" % (k, v))
        if len(self.trapv_scan_values):
            print("  Trap voltage scanning:")
            for k, v in self.trapv_scan_values.items():
                print("    %s => %s" % (trapv_tuple_to_str(k, self), v))
        if len(self.afg_volt_scan_values):
            print("  AFG voltage scanning:")
            for k, v in self.afg_volt_scan_values.items():
                print("    %s => %s" % (k, v))
        if len(self.afg_freq_scan_values):
            print("  AFG frequency scanning:")
            for k, v in self.afg_freq_scan_values.items():
                print("    %s => %s" % (k, v))
        
        if self.caen_tdc_error:
            print("  **** CAEN TDC error(s) ****")
            for err in self.caen_tdc_error.split("; "):
                print("    %s" % err)
        
        if self.caen_t0_offset_ms is None:
            print("  CAEN T0 offset unknown")
        else:
            print("  CAEN T0 offset: %.3fms" % self.caen_t0_offset_ms)
        
        print("  %s raw CAEN TDC entries:" % len(self.caen_tdc_raw))
        for tdc in self.caen_tdc_raw:
            print("    %s" % tdc)
        
        print("  %s parsed CAEN TDC triggers:" % len(self.caen_tdc_parsed))
        for trig in self.caen_tdc_parsed:
            print("    %s" % trig)

        print("  %s VT2 TDC entries:" % len(self.vt2_data))
        for tdc in self.vt2_data:
            print("    %s" % tdc)

        print("  %s VT4 TDC entries:" % len(self.tdc_data))
        for tdc in self.tdc_data:
            print("    %s" % tdc)
            
        print("  %s LRS1190 position entries:" % len(self.pos_data))
        for pos in self.pos_data:
            print("    %s" % pos)
        
    def get_all_scanned_vars(self, runinfo):
        scan_vals = self.ppg_scan_values
        scan_vals.update(self.epics_scan_values)
        scan_vals.update({trapv_tuple_to_str(k, runinfo): v for k,v in self.trapv_scan_values.items()})
        scan_vals.update({"%s voltage" % k : v for k,v in self.afg_volt_scan_values.items()})
        scan_vals.update({"%s frequency" % k : v for k,v in self.afg_freq_scan_values.items()})
        return scan_vals

def trapv_tuple_to_str(trapv, runinfo):
    """
    Convert a 2-tuple of (PPG pulse name, Softdac/16ao16 channel ID) to a human-readable
    string. E.g. ("pulse_trapv_capture", 2) -> "Ring capture voltage"

    Args:

    * trapv (2-tuple of (str, int))
    * runinfo (`MPETRunInfo`) - Needed to look up Softdac/16ao16 channel names    
    """
    if trapv[0]:
        pulse = trapv[0].replace("pulse_trapv_", "")
        chan = trapv[1]

        if len(runinfo.trapv_channel_names) > chan and runinfo.trapv_channel_names[chan] != "":
            chan = runinfo.trapv_channel_names[chan]
        else:
            chan = "Chan %d" % chan
        return "%s %s voltage" % (chan, pulse)

    else:
        return ("")

def make_int(val):
    if isinstance(val, str):
        if val.startswith("0x"):
            return int(val, 16)
        else:
            return int(val)

    return val

def make_none_empty_string(str_list):
    return [v if v is not None else "" for v in str_list]

def get_mpet_run_info(odb_json, odb_json_end=None, constants=MPETConstants()):
    """
    Get the current scan configuration from the provided ODB.
    
    Args:
        
    * odb_json (dict) - BOR ODB dump
    * odb_json_end (dict) - EOR ODB dump
    * constants (`MPETConstants`)
    
    Returns:
        `MPETRunInfo`
    """
    if odb_json_end is None:
        odb_json_end = odb_json

    run_info = MPETRunInfo()
    run_info.run_number = odb_json["Runinfo"]["Run number"]
    run_info.run_start_time = make_int(odb_json["Runinfo"]["Start time binary"])
    run_info.ppg_program = odb_json["Equipment"]["PPGCompiler"]["Programming"]
        
    x_enabled = odb_json["Scanning"]["Global"]["Settings"]["Enable X"]
    y_enabled = odb_json["Scanning"]["Global"]["Settings"]["Enable Y"]
    z_enabled = "Enable Z" in odb_json["Scanning"]["Global"]["Settings"] and odb_json["Scanning"]["Global"]["Settings"]["Enable Z"]

    run_info.num_x_steps = odb_json["Scanning"]["Global"]["Settings"]["nX steps"] if x_enabled else 0
    run_info.num_y_steps = odb_json["Scanning"]["Global"]["Settings"]["nY steps"] if y_enabled else 0
    run_info.num_z_steps = odb_json["Scanning"]["Global"]["Settings"]["nZ steps"] if z_enabled else 0
    run_info.num_reps_at_step = odb_json["Scanning"]["Global"]["Settings"]["N cycles per step"]

    # 16AO16 replaced the Softdac in 2021 (immediate changeover)
    if "GSC_16ao16" in odb_json["Scanning"]:
        trapv = odb_json["Scanning"]["GSC_16ao16"]["Settings"]
    elif "Softdac" in odb_json["Scanning"]:
        trapv = odb_json["Scanning"]["Softdac"]["Settings"]
    else:
        trapv = {"X blocks": [None], "X chan index": [-1], "Y blocks": [None], "Y chan index": [-1], "Channel names": [""]}

    epics_settings = odb_json["Scanning"]["Epics"]["Settings"]
    epics_dev_list = odb_json["Scanning"]["Epics"]["Definitions"]["Demand device"]

    # Tektronix AFG replaced the Agilent in 2022 (allowed to run either for a while, so check
    # which frontend was running)
    client_names = [v["Name"] for v in odb_json["System"]["Clients"].values()]

    if "AFGTek" not in odb_json["Scanning"] or "AfgFrontend" in client_names:
        # Agilent AFG
        afgs_enabled = []
        
        if "AFG" in odb_json["Equipment"]:
            for d in ["Quad", "Dipole"]:
                if odb_json["Equipment"]["AFG"]["Settings"][d]["Enabled"]:
                    afgs_enabled.append(d)
            run_info.quad_freqs = odb_json_end["Equipment"]["AFG"]["Computed"]["Quad"]["Frequency list"]
            run_info.dipole_freqs = odb_json_end["Equipment"]["AFG"]["Computed"]["Dipole"]["Frequency list"]
            run_info.freq_scanned_in_ppg = True
            afg_freq_settings = None

            try:
                afg_volt_settings = odb_json["Scanning"]["AFG"]["Settings"]
            except:
                # Before voltage scanning
                afg_volt_settings = None
    else:
        # Tektronix AFG
        afgs_enabled = []
        for d in ["Quad", "Dipole"]:
            if odb_json["Equipment"]["AFGTek"]["Settings"][d]["Enabled"]:
                afgs_enabled.append(d)
        afg_volt_settings = odb_json["Scanning"]["AFGTek"]["Voltage"]
        afg_freq_settings = odb_json["Scanning"]["AFGTek"]["Frequency"]
        run_info.quad_freqs = odb_json_end["Equipment"]["AFGTek"]["Computed"]["Quad"]["Frequency list"]
        run_info.dipole_freqs = odb_json_end["Equipment"]["AFGTek"]["Computed"]["Dipole"]["Frequency list"]
        run_info.freq_scanned_in_ppg = False
    
    if not isinstance(run_info.quad_freqs, list):
        run_info.quad_freqs = [run_info.quad_freqs]

    if not isinstance(run_info.dipole_freqs, list):
        run_info.dipole_freqs = [run_info.dipole_freqs]

    if x_enabled:
        run_info.scan_ppg_x = make_none_empty_string(odb_json["Scanning"]["PPG"]["Settings"]["X paths"])
        run_info.scan_trapv_x = list(zip(make_none_empty_string(trapv["X blocks"]), [make_int(idx) for idx in trapv["X chan index"]]))
        run_info.scan_epics_x = [epics_dev_list[idx] if idx >= 0 else "" for idx in epics_settings["X devices"] ]
        
        if afg_volt_settings is not None:
            run_info.scan_afg_volt_x = ["%s voltage" % d for d in afg_volt_settings if afg_volt_settings[d]["Dimension"] == "X" and d in afgs_enabled]

        if afg_freq_settings is not None:
            run_info.scan_afg_freq_x = ["%s frequency" % d for d in afg_freq_settings if afg_freq_settings[d]["Dimension"] == "X" and d in afgs_enabled]
        
    if y_enabled:
        run_info.scan_ppg_y = make_none_empty_string(odb_json["Scanning"]["PPG"]["Settings"]["Y paths"])
        run_info.scan_trapv_y = list(zip(make_none_empty_string(trapv["Y blocks"]), [make_int(idx) for idx in trapv["Y chan index"]]))
        run_info.scan_epics_y = [epics_dev_list[idx] if idx >= 0 else "" for idx in epics_settings["Y devices"] ]

        if afg_volt_settings is not None:
            run_info.scan_afg_volt_y = ["%s voltage" % d for d in afg_volt_settings if afg_volt_settings[d]["Dimension"] == "Y" and d in afgs_enabled]

        if afg_freq_settings is not None:
            run_info.scan_afg_freq_y = ["%s frequency" % d for d in afg_freq_settings if afg_freq_settings[d]["Dimension"] == "Y" and d in afgs_enabled]

    if z_enabled:
        run_info.scan_ppg_z = make_none_empty_string(odb_json["Scanning"]["PPG"]["Settings"]["Z paths"])
        run_info.scan_trapv_z = list(zip(make_none_empty_string(trapv["Z blocks"]), [make_int(idx) for idx in trapv["Z chan index"]]))
        run_info.scan_epics_z = [epics_dev_list[idx] if idx >= 0 else "" for idx in epics_settings["Z devices"] ]

        if afg_volt_settings is not None:
            run_info.scan_afg_volt_z = ["%s voltage" % d for d in afg_volt_settings if afg_volt_settings[d]["Dimension"] == "Z" and d in afgs_enabled]

        if afg_freq_settings is not None:
            run_info.scan_afg_freq_z = ["%s frequency" % d for d in afg_freq_settings if afg_freq_settings[d]["Dimension"] == "Z" and d in afgs_enabled]

    run_info.trapv_channel_names = make_none_empty_string(trapv["Channel names"])

    try:
        t0_offsets = odb_json_end["Equipment"]["PPGCompiler"]["Computed"]["T0 offsets (ms)"]
    except KeyError:
        t0_offsets = {}
    
    run_info.ppg_t0_offset_order = [k for k in t0_offsets.keys() if "/key" not in k]
    t0_offsets_arr = [v for k,v in t0_offsets.items() if "/key" not in k]
    
    try:
        run_info.default_trap_time_ms = t0_offsets[constants.trap_time_ej_from_ppg_block.upper()] - t0_offsets[constants.trap_time_in_from_ppg_block.upper()]
    except (KeyError, ValueError, IndexError):
        run_info.default_trap_time_ms = None

    if "V1290" in odb_json["Scanning"]:
        run_info.caen_trig_window_width_ms = odb_json["Scanning"]["V1290"]["Settings"]["Window width (us)"] * 1e-3
        run_info.caen_trig_window_offset_ms = odb_json["Scanning"]["V1290"]["Settings"]["Window offset (signed!) (us)"] * 1e-3

    run_info.v1290_t0_offset_from_ppg_block_chosen = None

    try:
        # Figure out extra offset to apply to CAEN ToF calculations
        # (as it has a max range of 50us and we might want to measure
        # larger ToFs as a combination of "coarse timing from PPG" plus
        # "fine timing from CAEN").
        #
        # First figure out what the CAEN trigger was defined relative to in the PPG
        # program.
        for block_name in constants.v1290_t0_offset_from_ppg_block:
            try:
                caen_block = run_info.ppg_program[block_name]
                run_info.v1290_t0_offset_from_ppg_block_chosen = block_name
            except:
                continue
        
        if run_info.v1290_t0_offset_from_ppg_block_chosen is None:
            raise KeyError()
        
        if "time reference" not in caen_block or caen_block["time reference"] == "":
            # relative to previous block
            ppg_keys = list(run_info.ppg_program.keys())
            caen_key_idx = ppg_keys.index(run_info.v1290_t0_offset_from_ppg_block_chosen)
            run_info.caen_rel_to_pulse_name = ppg_keys[caen_key_idx - 1].upper()
        else:
            # explicit relative block
            run_info.caen_rel_to_pulse_name = caen_block["time reference"].upper().replace("_TSTART_", "").replace("_TEND_", "")

        idx_caen = run_info.ppg_t0_offset_order.index(run_info.v1290_t0_offset_from_ppg_block_chosen.upper())
        idx_rel = run_info.ppg_t0_offset_order.index(run_info.caen_rel_to_pulse_name)
        
        # Timestamp:     T_rel    T_win_start      T_trig     T_win_end
        # PPG offsets:   T0[rel]                   T0[caen]
        #            {            <-------- win_width -------->
        # Variables: {            <-- win_offset -->
        #            {   < t0_off >
        #
        # T_win_start = T_trig + win_offset (win_offset is negative for us)
        # t0_off      = T_win_start - T_rel 
        run_info.default_caen_t0_offset_ms = t0_offsets_arr[idx_caen] - t0_offsets_arr[idx_rel] + run_info.caen_trig_window_offset_ms
    except (KeyError, ValueError, IndexError):
        if run_info.v1290_t0_offset_from_ppg_block_chosen is None:
            # User explicitly disabled the extra ToF logic
            run_info.default_caen_t0_offset_ms = 0
        else:
            run_info.default_caen_t0_offset_ms = None
        
    return run_info

def get_mpet_event_info(midas_event, run_info, constants=MPETConstants()):
    """
    Get scan setup for this event.
    
    Args:
        
    * midas_event (`midas.event.MidasEvent`)
    * run_info (`MPETRunInfo`)
    * constants (`MPETConstants`) - if you want to override any of the defaults used
        when parsing the data
    """
    mpet_event = MPETEvent()
    
    if not midas_event.bank_exists("SCAN"):
        return None
    
    mpet_event.original_midas_event = midas_event

    if midas_event.bank_exists("ETMS"):
        # 1 ms precision event time
        mpet_event.event_time = midas_event.banks["ETMS"].data[0] / 1000.
    else:
        # 1 second precision event time
        mpet_event.event_time = midas_event.header.timestamp

    mpet_event.serial_number = midas_event.header.serial_number
    
    # Scan metadata
    mpet_event.loop_count = midas_event.banks["SCAN"].data[0]
    mpet_event.x_step = midas_event.banks["SCAN"].data[1]
    mpet_event.y_step = midas_event.banks["SCAN"].data[2]
    mpet_event.z_step = -1 if len(midas_event.banks["SCAN"].data) < 7 else midas_event.banks["SCAN"].data[6]
    mpet_event.rep = midas_event.banks["SCAN"].data[3]
       
    if midas_event.bank_exists("XPPG"):
        for i, path in enumerate(run_info.scan_ppg_x):
            if path != "" and path is not None:
                mpet_event.ppg_scan_values[path] = midas_event.banks["XPPG"].data[i]
                
    if midas_event.bank_exists("YPPG"):
        for i, path in enumerate(run_info.scan_ppg_y):
            if path != "" and path is not None:
                mpet_event.ppg_scan_values[path] = midas_event.banks["YPPG"].data[i]
                
    if midas_event.bank_exists("ZPPG"):
        for i, path in enumerate(run_info.scan_ppg_z):
            if path != "" and path is not None:
                mpet_event.ppg_scan_values[path] = midas_event.banks["ZPPG"].data[i]
                
    if midas_event.bank_exists("TPPG"):
        offsets_ms = midas_event.banks["TPPG"].data
        
        try:
            idx_ej = run_info.ppg_t0_offset_order.index(constants.trap_time_ej_from_ppg_block.upper())
            idx_in = run_info.ppg_t0_offset_order.index(constants.trap_time_in_from_ppg_block.upper())
            mpet_event.trap_time_ms = offsets_ms[idx_ej] - offsets_ms[idx_in]
        except (KeyError, ValueError, IndexError):
            mpet_event.trap_time_ms = run_info.default_trap_time_ms

        if run_info.v1290_t0_offset_from_ppg_block_chosen:
            try:
                idx_caen = run_info.ppg_t0_offset_order.index(run_info.v1290_t0_offset_from_ppg_block_chosen.upper())
                idx_rel = run_info.ppg_t0_offset_order.index(run_info.caen_rel_to_pulse_name)
                mpet_event.caen_t0_offset_ms = offsets_ms[idx_caen] - offsets_ms[idx_rel] + run_info.caen_trig_window_offset_ms
            except (KeyError, ValueError, IndexError):
                mpet_event.caen_t0_offset_ms = run_info.default_caen_t0_offset_ms
        else:
            mpet_event.caen_t0_offset_ms = run_info.default_caen_t0_offset_ms
    else:
        mpet_event.trap_time_ms = run_info.default_trap_time_ms
        mpet_event.caen_t0_offset_ms = run_info.default_caen_t0_offset_ms
        
    if midas_event.bank_exists("XEPD"):
        for i, dev in enumerate(run_info.scan_epics_x):
            if dev != "" and dev is not None:
                mpet_event.epics_scan_values[dev] = midas_event.banks["XEPD"].data[i]
                
    if midas_event.bank_exists("YEPD"):
        for i, dev in enumerate(run_info.scan_epics_y):
            if dev != "" and dev is not None:
                mpet_event.epics_scan_values[dev] = midas_event.banks["YEPD"].data[i]
                
    if midas_event.bank_exists("ZEPD"):
        for i, dev in enumerate(run_info.scan_epics_z):
            if dev != "" and dev is not None:
                mpet_event.epics_scan_values[dev] = midas_event.banks["ZEPD"].data[i]
        
    if midas_event.bank_exists("XSFT"):
        for i, (block, chan) in enumerate(run_info.scan_trapv_x):
            if block != "" and block is not None:
                mpet_event.trapv_scan_values[(block, chan)] = midas_event.banks["XSFT"].data[i]
        
    if midas_event.bank_exists("YSFT"):
        for i, (block, chan) in enumerate(run_info.scan_trapv_y):
            if block != "" and block is not None:
                mpet_event.trapv_scan_values[(block, chan)] = midas_event.banks["YSFT"].data[i]
        
    if midas_event.bank_exists("ZSFT"):
        for i, (block, chan) in enumerate(run_info.scan_trapv_z):
            if block != "" and block is not None:
                mpet_event.trapv_scan_values[(block, chan)] = midas_event.banks["ZSFT"].data[i]
        
    if midas_event.bank_exists("XGSC"):
        for i, (block, chan) in enumerate(run_info.scan_trapv_x):
            if block != "" and block is not None:
                mpet_event.trapv_scan_values[(block, chan)] = midas_event.banks["XGSC"].data[i]
        
    if midas_event.bank_exists("YGSC"):
        for i, (block, chan) in enumerate(run_info.scan_trapv_y):
            if block != "" and block is not None:
                mpet_event.trapv_scan_values[(block, chan)] = midas_event.banks["YGSC"].data[i]

    if midas_event.bank_exists("ZGSC"):
        for i, (block, chan) in enumerate(run_info.scan_trapv_z):
            if block != "" and block is not None:
                mpet_event.trapv_scan_values[(block, chan)] = midas_event.banks["ZGSC"].data[i]

    if midas_event.bank_exists("VOLQ"):
        mpet_event.afg_volt_scan_values["Quad"] = midas_event.banks["VOLQ"].data[0]

    if midas_event.bank_exists("VOLD"):
        mpet_event.afg_volt_scan_values["Dipole"] = midas_event.banks["VOLD"].data[0]

    if midas_event.bank_exists("FRQQ"):
        # Freq being scanned like any other var (rather than within PPG program)
        mpet_event.afg_freq_scan_values["Quad"] = midas_event.banks["FRQQ"].data[0]

    if midas_event.bank_exists("FRQD"):
        # Freq being scanned like any other var (rather than within PPG program)
        mpet_event.afg_freq_scan_values["Dipole"] = midas_event.banks["FRQD"].data[0]
    
    if constants.acc_time_odb_path in mpet_event.ppg_scan_values:
        # Accumulation time is being scanned
        mpet_event.accumulation_time_ms = mpet_event.ppg_scan_values[constants.acc_time_odb_path]
    elif constants.acc_time_block_name in run_info.ppg_program:
        # Accumulation time is constant
        mpet_event.accumulation_time_ms = run_info.ppg_program[constants.acc_time_block_name]["pulse width (ms)"]
    else:
        mpet_event.accumulation_time_ms = None
    
    # TDC data
              
    if midas_event.bank_exists("DVT4"):
        t_gate_beg = None
        
        # DVT4 bank contains time data from VT4
        num_words = len(midas_event.banks["DVT4"].data)
        
        ppg_cycle = midas_event.banks["DVT4"].data[0]
        
        # Skip first word, as it's metadata not timestamps
        for i in range(1, num_words, 2):
            tdc = TDC()
            tdc.ppg_cycle = ppg_cycle
            
            lo = midas_event.banks["DVT4"].data[i]
            hi = midas_event.banks["DVT4"].data[i+1]
            
            hitime  = hi & 0xFFFF
            tdc.count = (hi & 0x03FF0000) >> 16
            chanbp    = (hi & 0x3C000000) >> 26
            new_cycle = (hi & 0x80000000) >> 31
            gate_rise = (hi & 0x40000000) >> 30
            
            tottime   = lo + (hitime << 32)
            tdc.time_secs = tottime * pow(10,-8)  # 100MHz clock
    
            if new_cycle:
                tdc.event_type = TDCEventType.new_cycle
                
            if gate_rise and chanbp == 0:
                tdc.event_type = TDCEventType.gate_start
                t_gate_beg = tdc.time_secs
           
            if chanbp:
                tdc.event_type = TDCEventType.timestamp
    
                for c in range(4):
                    if chanbp & (1 << c):
                        tdc.channel_ids.append(c)
                                 
            if not gate_rise and not new_cycle and not chanbp:
                tdc.event_type = TDCEventType.gate_end
            
            if t_gate_beg is not None:
                tdc.tof_secs = tdc.time_secs - t_gate_beg
                            
            mpet_event.tdc_data.append(tdc)
    
    if midas_event.bank_exists("DVT2"):
        t_gate_beg = None
        
        # DVT2 bank contains time data from VT2
        num_words = len(midas_event.banks["DVT2"].data)
        
        ppg_cycle = midas_event.banks["DVT2"].data[0]
        prev_count = -1
        
        # Skip first word, as it's metadata not timestamps
        for i in range(1, num_words, 2):
            tdc = TDC()
            tdc.ppg_cycle = ppg_cycle
            
            lo = midas_event.banks["DVT2"].data[i+1]
            hi = midas_event.banks["DVT2"].data[i]
            
            hitime = hi & 0xFFFF
            
            tdc.count  = (hi & 0x03FF0000) >> 16
            chanbp     = (hi & 0x60000000) >> 29
            gate_start = (hi & 0x80000000) >> 31
            gate_end   = (hi & 0x10000000) >> 28
            
            tottime    = lo + (hitime << 32)
            tdc.time_secs = tottime * pow(10,-8)  # 100MHz clock
    
            if prev_count != tdc.count:
                tdc.event_type = TDCEventType.new_cycle
                prev_count = tdc.count
                
            if gate_start:
                tdc.event_type = TDCEventType.gate_start
                t_gate_beg = tdc.time_secs
           
            if chanbp:
                tdc.event_type = TDCEventType.timestamp
    
                for c in range(2):
                    if chanbp & (1 << c):
                        tdc.channel_ids.append(c)
                                 
            if gate_end:
                tdc.event_type = TDCEventType.gate_end
            
            if t_gate_beg is not None:
                tdc.tof_secs = tdc.time_secs - t_gate_beg
                            
            mpet_event.vt2_data.append(tdc)
        
    # CAEN 25ps TDC data
    
    if midas_event.bank_exists("25PS"):
        caen_errors = []
        num_words = len(midas_event.banks["25PS"].data)
        trigger_count = -1
        ettt_bins = {}
        timestamps_per_trig = {}
        ppg_cycle = midas_event.banks["25PS"].data[0]
        
        for i in range(1, num_words):
            datum = midas_event.banks["25PS"].data[i]
            
            if (datum & 0xf8000000) == 0x40000000:
                # Global header
                trigger_count = (datum >> 5) & 0x3FFFFF
                timestamps_per_trig[trigger_count] = []
            elif (datum & 0xf8000000) == 0x80000000:
                # Global trailer
                if datum & 0x07000000:
                    # Global trailer has an error
                    if (datum >> 24) & 0x1:
                        caen_errors.append("Global trailer reports TDC error")
                    if (datum >> 25) & 0x1:
                        caen_errors.append("Global trailer reports event buffer overflow")
                    if (datum >> 26) & 0x1:
                        caen_errors.append("Global trailer reports triggers lost")
                
                # 5 LSB of ETTT
                if trigger_count in ettt_bins:
                    ettt_bins[trigger_count] |= (datum & 0x1F)
            elif (datum & 0xf8000000) == 0x08000000:
                # TDC header
                pass
            elif (datum & 0xf8000000) == 0x18000000:
                # TDC trailer
                pass
            elif (datum & 0xf8000000) == 0x88000000:
                # Extended trigger time tag
                ettt_bins[trigger_count] = (datum & 0x07FFFFFF) << 5
            elif (datum & 0xf8000000) == 0x20000000:
                # TDC error
                tdc_id = (datum >> 24) & 0x3
                
                if datum & 0x1:
                    caen_errors.append("TDC %d reports hit lost on group 0 due to readout FIFO" % tdc_id)
                if (datum >> 1) & 0x1:
                    caen_errors.append("TDC %d reports hit lost on group 0 due to L1 buffer" % tdc_id)
                if (datum >> 2) & 0x1:
                    caen_errors.append("TDC %d reports hit error on group 0" % tdc_id)
                if (datum >> 3) & 0x1:
                    caen_errors.append("TDC %d reports hit lost on group 1 due to readout FIFO" % tdc_id)
                if (datum >> 4) & 0x1:
                    caen_errors.append("TDC %d reports hit lost on group 1 due to L1 buffer" % tdc_id)
                if (datum >> 5) & 0x1:
                    caen_errors.append("TDC %d reports hit error on group 1" % tdc_id)
                if (datum >> 6) & 0x1:
                    caen_errors.append("TDC %d reports hit lost on group 2 due to readout FIFO" % tdc_id)
                if (datum >> 7) & 0x1:
                    caen_errors.append("TDC %d reports hit lost on group 2 due to L1 buffer" % tdc_id)
                if (datum >> 8) & 0x1:
                    caen_errors.append("TDC %d reports hit error on group 2" % tdc_id)
                if (datum >> 9) & 0x1:
                    caen_errors.append("TDC %d reports hit lost on group 3 due to readout FIFO" % tdc_id)
                if (datum >> 10) & 0x1:
                    caen_errors.append("TDC %d reports hit lost on group 3 due to L1 buffer" % tdc_id)
                if (datum >> 11) & 0x1:
                    caen_errors.append("TDC %d reports hit error on group 3" % tdc_id)
                if (datum >> 12) & 0x1:
                    caen_errors.append("TDC %d reports event size limit reached" % tdc_id)
                if (datum >> 13) & 0x1:
                    caen_errors.append("TDC %d reports event lost due to trigger FIFO overflow" % tdc_id)
                if (datum >> 14) & 0x1:
                    caen_errors.append("TDC %d reports fatal chip error" % tdc_id)
            elif (datum & 0xf8000000) == 0x00000000:
                # TDC measurement
                timestamp = datum & 0x1FFFFF
                channel = (datum >> 21) & 0x1F
                is_trailing_edge = (datum >> 25) & 0x1
                tdc = CaenTDCRaw()
                tdc.ppg_cycle = ppg_cycle
                tdc.trigger_count = trigger_count
                tdc.channel_id = channel
                tdc.timestamp_secs = timestamp * constants.v1290_bin_width_ps / 1e12
                timestamps_per_trig[trigger_count].append(tdc)
            else:
                raise ValueError("Unexpected V1290 datum 0x%08x" % datum)
        
        ettt_secs = {}

        for trig, ettt in ettt_bins.items():
            ettt_secs[trig] = ettt * constants.v1290_ettt_lsb_ns / 1e9

        if len(ettt_secs):
            first_trigger_count = min(ettt_secs.keys())
            mpet_event.caen_first_ettt_secs = ettt_secs[first_trigger_count]
        
        for trigger_count, timestamps in timestamps_per_trig.items():
            for tdc in timestamps:
                if tdc.trigger_count in ettt_secs:
                    tdc.ettt_secs = ettt_secs[tdc.trigger_count]

                mpet_event.caen_tdc_raw.append(tdc)

        if len(caen_errors):
            mpet_event.caen_tdc_error = "; ".join(caen_errors)
                
                
        # Parse the CAEN TDC data
        # First agrregate timestamps per trigger
        curr_trig_id = None
        parsed = []
        
        for tdc in mpet_event.caen_tdc_raw:
            if curr_trig_id != tdc.trigger_count:
                curr_trig_id = tdc.trigger_count
                parsed.append({"obj": CaenTDC(), "chan_ts": {}})
                parsed[-1]["obj"].trigger_count = curr_trig_id
                
            if tdc.channel_id not in parsed[-1]["chan_ts"]:
                parsed[-1]["chan_ts"][tdc.channel_id] = []
            
            parsed[-1]["chan_ts"][tdc.channel_id].append(tdc.timestamp_secs)
        
        # Now compute ToF and positions if needed
        for p in parsed:
            obj = p["obj"]
            trig_data = p["chan_ts"]
            trig_id = obj.trigger_count
            x1 = []
            x2 = []
            y1 = []
            y2 = []
            mcp_tof = []
                
            t0_offset_ms = mpet_event.caen_t0_offset_ms
                    
            for chan_id, chan_data in trig_data.items():
                for ts in chan_data:
                    tof = None if t0_offset_ms is None else ts + (t0_offset_ms / 1000.)

                    if tof is not None and mpet_event.caen_first_ettt_secs is not None and trig_id in ettt_secs:
                        tof += ettt_secs[trig_id] - mpet_event.caen_first_ettt_secs

                        if mpet_event.caen_first_ettt_secs > ettt_secs[trig_id]:
                            # ETTT rollover every 107.4s
                            tof += 2^32 * constants.v1290_ettt_lsb_ns / 1e9
                    
                    if chan_id not in obj.all_tof_secs:
                        obj.all_tof_secs[chan_id] = []
                        
                    obj.all_tof_secs[chan_id].append(tof)
                    
                    if t0_offset_ms is not None:
                        # First timestamp on some channels has special significance
                        if chan_id == constants.v1290_chan_mcp_fast:
                            mcp_tof.append(tof)
                        if chan_id == constants.v1290_chan_mcp_x1:
                            x1.append(tof)
                        if chan_id == constants.v1290_chan_mcp_x2:
                            x2.append(tof)
                        if chan_id == constants.v1290_chan_mcp_y1:
                            y1.append(tof)
                        if chan_id == constants.v1290_chan_mcp_y2:
                            y2.append(tof)

            obj.mcp_tof_secs = mcp_tof

            if len(x1) != len(mcp_tof) or len(x1) != len(x2) or len(x1) != len(y1) or len(x2) != len(y2):
                obj.failed_to_compute_positions = True
            else:
                # Convert timestamps to positions 
                for i in range(len(x1)):
                    if x1[i] is not None and x2[i] is not None:
                        x_pos = (x2[i] - x1[i]) * 1e9 / constants.dld40_ns_per_mm_x + constants.dld40_offset_mm_x
                        obj.pos_x_mm.append(x_pos)
                    if y1[i] is not None and y2[i] is not None:
                        y_pos = (y2[i] - y1[i]) * 1e9 / constants.dld40_ns_per_mm_y + constants.dld40_offset_mm_y
                        obj.pos_y_mm.append(y_pos)
            
            mpet_event.caen_tdc_parsed.append(obj)
                
    # Position data for old MCP
    
    if midas_event.bank_exists("MCPP"):
        num_words = len(midas_event.banks["MCPP"].data)
        
        ppg_cycle = midas_event.banks["MCPP"].data[0]
        
        for i in range(1, num_words):
            x = (midas_event.banks["MCPP"].data[i] >> 8) & 0xFF
            y = midas_event.banks["MCPP"].data[i] & 0xFF
            pos = Position(ppg_cycle, x, y, constants)
            mpet_event.pos_data.append(pos)
                
    return mpet_event

    
