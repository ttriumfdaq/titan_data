import titan_data.mpet
import argparse
import statistics

def handle_file(file_name):
    reader = titan_data.mpet.MPETFile(file_name)
    asum_chan = 0
    caen_center_chan = 1
    hits_vs_freq = []
    hits_vs_x = []
    hits_vs_y = []
    hits_vs_z = []

    for ev in reader:
        num_freqs = -1
        last_count = -1

        while len(hits_vs_x) <= ev.x_step:
            hits_vs_x.append([])
        while len(hits_vs_y) <= ev.y_step:
            hits_vs_y.append([])
        while len(hits_vs_z) <= ev.z_step:
            hits_vs_z.append([])

        for tdc in ev.caen_tdc_parsed:
            for tof_secs in tdc.mcp_tof_secs:
                tof_us = tof_secs * 1e6

                if ev.x_step >= 0:
                    hits_vs_x[ev.x_step].append(tof_us)
                if ev.y_step >= 0:
                    hits_vs_y[ev.y_step].append(tof_us)
                if ev.z_step >= 0:
                    hits_vs_z[ev.z_step].append(tof_us)

        for tdc in ev.vt2_data:
            if tdc.event_type != titan_data.mpet.TDCEventType.new_cycle:
                if tdc.count != last_count:
                    num_freqs += 1
                    last_count = tdc.count
            
            while len(hits_vs_freq) <= num_freqs:
                hits_vs_freq.append([])

            if tdc.event_type == titan_data.mpet.TDCEventType.timestamp and asum_chan in tdc.channel_ids and tdc.tof_secs is not None:
                hits_vs_freq[num_freqs].append(tdc.tof_secs * 1e6)
                
                if ev.x_step >= 0:
                    hits_vs_x[ev.x_step].append(tdc.tof_secs * 1e6)
                if ev.y_step >= 0:
                    hits_vs_y[ev.y_step].append(tdc.tof_secs * 1e6)
                if ev.z_step >= 0:
                    hits_vs_z[ev.z_step].append(tdc.tof_secs * 1e6)

    print("Mean ToF (us) for each freq idx:")
    for shotnum, fullhist in enumerate(hits_vs_freq):
        if len(fullhist) == 0:
            print("%03d: No data" % shotnum)
        else:
            mean = statistics.mean(fullhist)
            print("%03d: %.1f %s" % (shotnum, mean, "-" * int(mean/3)))

    print("Mean ToF (us) for each X idx:")
    for x, fullhist in enumerate(hits_vs_x):
        if len(fullhist) == 0:
            print("%03d: No data" % x)
        else:
            mean = statistics.mean(fullhist)
            print("%03d: %.1f %s" % (x, mean, "-" * int(mean/3)))

    print("Mean ToF (us) for each Y idx:")
    for y, fullhist in enumerate(hits_vs_y):
        if len(fullhist) == 0:
            print("%03d: No data" % y)
        else:
            mean = statistics.mean(fullhist)
            print("%03d: %.1f %s" % (y, mean, "-" * int(mean/3)))

    print("Mean ToF (us) for each Z idx:")
    for z, fullhist in enumerate(hits_vs_z):
        if len(fullhist) == 0:
            print("%03d: No data" % z)
        else:
            mean = statistics.mean(fullhist)
            print("%03d: %.1f %s" % (z, mean, "-" * int(mean/3)))
 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("midas_file")
    args = parser.parse_args()

    handle_file(args.midas_file)