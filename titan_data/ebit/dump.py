"""
Tool that will print EBIT data to screen.

If an argument is provided, we open that midas file.
If no argument is provided, we connect to the live midas experiment.
"""

try:
    import midas
    import midas.client
    got_client = True
except (ImportError, SyntaxError, EnvironmentError):
    import titan_data.midas as midas
    got_client = False
    
import titan_data.ebit
import sys
import time
import datetime
import argparse
 
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Dump MPET data to screen')
    file_help = "Midas file must be provided"
    file_nargs = 1
    if got_client:
        file_help = "If midas file not provided, connects to live experiment"
        file_nargs = '?'
    parser.add_argument('filename', metavar="midas_file", nargs=file_nargs, help=file_help)
    parser.add_argument('--summary', action="store_true",  help="Print a summary rather than individual events")
    args = parser.parse_args()
    
    if args.filename is not None:
        # Print from file
        file_path = args.filename[0] if file_nargs == 1 else args.filename
        
        if args.summary:
            summary = titan_data.ebit.EBITSummary(file_path)
            summary.dump_to_screen()
        else:
            reader = titan_data.ebit.EBITFile(file_path)
        
            for ebit_event in reader:
                ebit_event.dump_to_screen()
                
            if reader.run_comment is not None:
                print("")
                print("Run comments:")
                print("*" * 78)
                print(reader.run_comment)
                print("*" * 78)
    else:
        # Print live events
        if not got_client:
            print("Midas must be installed (and the full midas python packages available) for live events to be dumped")
            exit(0)

        client = midas.client.MidasClient("ebitdump")
        buffer_handle = client.open_event_buffer("SYSTEM")
        client.register_event_request(buffer_handle, sampling_type=midas.GET_ALL)
        
        curr_run = None
        
        while True:
            # Reload run-level settings if needed
            new_run = client.odb_get("/Runinfo/Run number")
            
            if new_run != curr_run:
                run_info = titan_data.ebit.get_ebit_run_info(client.odb_get("/"))
                curr_run = new_run
            
            # Handle any events
            midas_event = client.receive_event(buffer_handle, True)
            
            if midas_event:
                ebit_event = titan_data.ebit.get_ebit_event_info(midas_event, run_info)
                
                if ebit_event:
                    ebit_event.dump_to_screen()
                
            client.communicate(100)
