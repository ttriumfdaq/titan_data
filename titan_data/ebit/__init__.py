"""
Tools for parsing EBIT data.


Example usage for iterating over events in python:

```
import titan_data.ebit

ebit_file = titan_data.ebit.EBITFile("/path/to/midas_file.mid")

for ebit_event in ebit_file:
    print("FC3 reading was %s" % ebit_event.faraday_cup_readings["ILE2T:FC3:SCALECUR"])
```



Example usage for grabbing all events as a list:

```
import titan_data.ebit

ebit_file = titan_data.ebit.EBITFile("/path/to/midas_file.mid")
ebit_events = ebit_file.get_all_event_info()

for ebit_event in ebit_events:
    print("FC3 reading was %s" % ebit_event.faraday_cup_readings["ILE2T:FC3:SCALECUR"])
```
"""

import titan_data.midas as midas
import titan_data.midas.file_reader as file_reader
import datetime

class EBITSummary:
    """
    Class that parses a full EBIT midas file and summarizes the data:
    * Sums the MCA4 data
    * Makes lists of Faraday cup readings
    
    Separate sums/lists are created for the different scan configurations
    if scanning is enabled (e.g. the MCA data with a PPG offset of 2ms is
    summed separately from the data with an offset of 5ms).
    
    If you don't like the summary info presented, you can create an EBITFile
    object instead and iterate over the events manually.
    
    Members:
    
    * mca_summary (list of dicts) - Dict contents are:
        > x (int) - X step of scan
        > y (int) - Y step of scan
        > z (int) - Z step of scan
        > fc0_inout (str) - Whether FC0 was In or Out
        > ppg (dict of {str: number}) - Value of any PPG variables being scanned
        > epics (dict of {str: number}) - Value of any EPICS variables being scanned
        > fcups {dict of {str: list of number) - FC0/FC3 measurements taken. Key is EPICS PV name.
        > chan_1 {list of int} - MCA4 channel 1 histogram (summed from whole run)
        > chan_2 {list of int} - MCA4 channel 2 histogram (summed from whole run)
    * run_info (EBITRunInfo) - Run-level information (e.g. start time, MCA bin width, etc)
    * run_comment (str) - Comments provided by user during run
    """
    def __init__(self, file_path):
        ebitfile = EBITFile(file_path)
        self.mca_summary = []
        
        mca4_totals_dict = {}
        
        for ev in ebitfile:
            if ev.x_step not in mca4_totals_dict:
                mca4_totals_dict[ev.x_step] = {}
                
            if ev.y_step not in mca4_totals_dict[ev.x_step]:
                mca4_totals_dict[ev.x_step][ev.y_step] = {}
                
            if ev.z_step not in mca4_totals_dict[ev.x_step][ev.y_step]:
                mca4_totals_dict[ev.x_step][ev.y_step][ev.z_step] = {}
                
            if ev.fc0_inout not in mca4_totals_dict[ev.x_step][ev.y_step][ev.z_step]:
                mca4_totals_dict[ev.x_step][ev.y_step][ev.z_step][ev.fc0_inout] = {
                                                                        "x": ev.x_step,
                                                                        "y": ev.y_step,
                                                                        "z": ev.z_step,
                                                                        "fc0_inout": ev.fc0_inout,
                                                                        "ppg": ev.ppg_scan_values,
                                                                        "epics": ev.epics_scan_values,
                                                                        "fcups": {},
                                                                        "chan_1": list(ev.mca4_chan1),
                                                                        "chan_2": list(ev.mca4_chan2)}
            else:
                for i, v in enumerate(ev.mca4_chan1):
                    mca4_totals_dict[ev.x_step][ev.y_step][ev.z_step][ev.fc0_inout]["chan_1"][i] += v
                    
                for i, v in enumerate(ev.mca4_chan2):
                    mca4_totals_dict[ev.x_step][ev.y_step][ev.z_step][ev.fc0_inout]["chan_2"][i] += v
                    
            for cup, value in ev.faraday_cup_readings.items():
                if cup not in mca4_totals_dict[ev.x_step][ev.y_step][ev.z_step][ev.fc0_inout]["fcups"]:
                    mca4_totals_dict[ev.x_step][ev.y_step][ev.z_step][ev.fc0_inout]["fcups"][cup] = [value]
                else:
                    mca4_totals_dict[ev.x_step][ev.y_step][ev.z_step][ev.fc0_inout]["fcups"][cup].append(value)
        
        for x in mca4_totals_dict.values():
            for y in x.values():
                for z in y.values():
                    for f in z.values():
                        self.mca_summary.append(f)
        
        self.run_info = ebitfile.run_info
        self.run_comment = ebitfile.run_comment

    def dump_to_screen(self):
        print("")
        print("Run info:")
        print("=" * 78)
        
        print("Run number            : %s" % self.run_info.run_number)
        print("Run start time        : %s (%s)" % (self.run_info.run_start_time, datetime.datetime.fromtimestamp(self.run_info.run_start_time)))
        print("MCA bin width (ns)    : %s" % self.run_info.mca4_bin_width_ns)
        print("Ion                   : %s" % self.run_info.ion)
        print("MCP                   : %s" % self.run_info.mcp)
        print("Electron current (mA) : %s" % self.run_info.electron_current_mA)
        
        if self.run_comment:
            print("")
            print("Run comments:")
            print("=" * 78)
            print(self.run_comment)
        
        print("")
        print("Data summary:")
        print("=" * 78)
        
        for data in self.mca_summary:
            print("X point %d, Y point %d, Z point %d, FC0 %s" % (data["x"], data["y"], data["z"], data["fc0_inout"]))
            
            if len(data["ppg"]):
                print("  PPG scanning:")
                for k, v in data["ppg"].items():
                    print("    %s => %s" % (k, v))
            if len(data["epics"]):
                print("  EPICS scanning:")
                for k, v in data["epics"].items():
                    print("    %s => %s" % (k, v))
            
            for mca_chan in ["1", "2"]:        
                max_val = 0
                max_bin = None
                    
                for i, v in enumerate(data["chan_%s" % mca_chan]):
                    if v > max_val:
                        max_bin = i
                        max_val = v
            
                print("  MCA4 channel %s has %s bins. Max bin is %s with %s counts" % (mca_chan, len(data["chan_%s" % mca_chan]), max_bin, max_val))
 
            for cup, cup_data in data["fcups"].items():
                num_val = len(cup_data)
                
                if num_val > 0:
                    max_val = max(cup_data)
                    min_val = min(cup_data)
                    avg_val = sum(cup_data) / num_val
                else:
                    max_val = None
                    min_val = None
                    avg_val = None
                    
                print("  Faraday cup %s has %s readings. Min/Avg/Max values are %s/%s/%s" % (cup, num_val, min_val, avg_val, max_val))
 
class EBITFile:
    """
    Class that provides easy access to EBIT-specific data .
    
    run_comment is only populated once we encounter the special
    event that contains the run comment. This is usually the last
    event in the file. You will probably have to read ALL the event
    in the file before you find the comment!
    """
    def __init__(self, file_path):
        self.midas_file = file_reader.MidasFile(file_path)
        self.run_info = get_ebit_run_info(self.midas_file.get_bor_odb_dump().data)
        self.run_comment = None
        
    def get_all_event_info(self):
        """
        Returns data for all events in a file as a list.
        This may be slow and memory-inefficient if you have a large number
        of events in the file.
        """
        return [ev for ev in self]

    def __next__(self):
        """
        Iterable interface for looping through events.
        """
        while True:
            midas_event = self.midas_file.read_next_event()
            
            if midas_event is None:
                # End of file - stop iterating
                raise StopIteration()
            
            # Try to convert to an EBITEvent
            ebit_event = get_ebit_event_info(midas_event, self.run_info)
            
            if ebit_event is None:
                # Internal midas event or run comment event; move on to next real event
                if midas_event.bank_exists("NOTE"):
                    self.run_comment = bytes(midas_event.banks["NOTE"].data).decode('utf-8')
                    
                continue
                
            return ebit_event
    
    next = __next__ # for Python 2        
        
    def __iter__(self):
        """
        Iterable interface for looping through events.
        """
        return self

class EBITRunInfo:
    def __init__(self):
        """
        Overall configuration of voltage/EPICS scanning etc.
        
        If scanning is disabled, most of these lists will be empty.
        
        Members:
            
        * run_number (int) - Midas run number
        * run_start_time (int) - UNIX timestamp when run started
        * num_x_steps (int) - Number of X steps in scan (data is taken at N+1 points)
        * num_y_steps (int) - Number of Y steps in scan (data is taken at N+1 points)
        * scan_ppg_x (list of str) - list of ODB paths changed in each X step
        * scan_ppg_y (list of str) - list of ODB paths changed in each Y step
        * scan_ppg_z (list of str) - list of ODB paths changed in each Z step
        * scan_epics_x (list of str) - list of devices changed in each X step
        * scan_epics_y (list of str) - list of devices changed in each Y step
        * scan_epics_z (list of str) - list of devices changed in each Z step
        * flip_bank_order (list of string) - Order of "flippable" devices reported in the FLIP bank
        * flip_state_meanings (dict of {str: {True: str, False: str}} - Human readable meanings of True/False 
            as reported in FLIP bank for each flippable device
        * initial_fc0inout_state (str) - Initial state of FC0 at begin of run
        * faraday_cup_pvs (list of str) - EPICS PV names being logged at each cycle
        * mca4_bin_width_ns (int) - How wide each bin in the MCA4 histograms is, in ns
        * ion (string) - Ion that user stated when starting the run
        * mcp (string) - MCP# that user stated when starting the run
        * electron_current_mA (number) - Electron current in mA that user stated when starting the run
        * ppg_program (dict) - PPG program being executed, as recorded in the ODB
        """
        self.run_number = None
        self.run_start_time = None
        self.num_x_steps = 0
        self.num_y_steps = 0
        self.scan_ppg_x = []
        self.scan_ppg_y = []
        self.scan_ppg_z = []
        self.scan_epics_x = []
        self.scan_epics_y = []
        self.scan_epics_z = []
        self.flip_bank_order = []
        self.flip_state_meanings = {}
        self.initial_fc0inout_state = None
        self.faraday_cup_pvs = []
        self.mca4_bin_width_ns = 0
        self.ion = ""
        self.mcp = ""
        self.electron_current_mA = 0
        self.ppg_program = None

    def get_all_scanned_var_names(self):
        x_keys = []
        y_keys = []
        z_keys = []

        if self.num_x_steps > 0:
            x_keys.extend([p for p in self.scan_ppg_x if p != ""])
            x_keys.extend([p for p in self.scan_epics_x if p != ""])

        if self.num_y_steps > 0:
            y_keys.extend([p for p in self.scan_ppg_y if p != ""])
            y_keys.extend([p for p in self.scan_epics_y if p != ""])

        if self.num_z_steps > 0:
            z_keys.extend([p for p in self.scan_ppg_z if p != ""])
            z_keys.extend([p for p in self.scan_epics_z if p != ""])

        return (x_keys, y_keys, z_keys)

class EBITEvent:
    """
    Stores data from a single PPG cycle.
    
    Members:
    
    * event_time (int) - UNIX timestamp when event was read out
    * loop_count (int) - Overall loop number (number of scan sweeps completed)
    * x_step (int) - X step of the current loop
    * y_step (int) - Y step of the current loop
    * z_step (int) - Y step of the current loop
    * ppg_scan_values (dict of {str: float}) - For PPG values that are being scanned, ODB path -> value
    * epics_scan_values (dict of {str: float}) - For EPICS values that are being scanned, device name -> value
    * fc0_inout (str) - Whether Faraday cup FC0 was "In" or "Out".
    * faraday_cup_readings (dict of {str: float}) - {EPICS PV name: reading} for Faraday cups
    * mca4_chan1 (list of int) - Histogram from STOP1 of MCA4
    * mca4_chan2 (list of int) - Histogram from STOP2 of MCA4
    * original_midas_event (`midas.event.MidasEvent`)
    """
    def __init__(self):
        self.event_time = None
        
        self.loop_count = 0
        self.x_step = 0
        self.y_step = 0
        self.z_step = 0
        
        self.ppg_scan_values = {} 
        self.epics_scan_values = {}
        
        self.fc0_inout = False
        self.faraday_cup_readings = {}
        
        self.mca4_chan1 = []
        self.mca4_chan2 = []
        
        self.original_midas_event = None
        
    def get_all_scanned_vars(self):
        scan_vals = self.ppg_scan_values
        scan_vals.update(self.epics_scan_values)
        return scan_vals

    def dump_to_screen(self):
        print("Event # %s, at %s" % (self.original_midas_event.header.serial_number, datetime.datetime.fromtimestamp(self.event_time)))
        print("  Loop count %d, X point %d, Y point %d, Z point %d, FC0 %s" % (self.loop_count, self.x_step, self.y_step, self.z_step, self.fc0_inout))
        
        if len(self.ppg_scan_values):
            print("  PPG scanning:")
            for k, v in self.ppg_scan_values.items():
                print("    %s => %s" % (k, v))
        if len(self.epics_scan_values):
            print("  EPICS scanning:")
            for k, v in self.epics_scan_values.items():
                print("    %s => %s" % (k, v))
        if len(self.faraday_cup_readings):
            print("  Faraday cup readings:")
            for k, v in self.faraday_cup_readings.items():
                print("    %s => %s" % (k, v))
                
        max_val_chan1 = 0
        max_val_chan2 = 0
        max_bin_chan1 = None
        max_bin_chan2 = None
        
        for i, v in enumerate(self.mca4_chan1):
            if v > max_val_chan1:
                max_bin_chan1 = i
                max_val_chan1 = v
            
        for i, v in enumerate(self.mca4_chan2):
            if v > max_val_chan2:
                max_bin_chan2 = i
                max_val_chan2 = v
        
        print("  MCA4 channel 1 has %s bins. Max bin is %s with %s counts" % (len(self.mca4_chan1), max_bin_chan1, max_val_chan1))
        print("  MCA4 channel 2 has %s bins. Max bin is %s with %s counts" % (len(self.mca4_chan2), max_bin_chan2, max_val_chan2))
            
def make_int(val):
    if isinstance(val, str):
        if val.startswith("0x"):
            return int(val, 16)
        else:
            return int(val)

    return val
        
def make_none_empty_string(str_list):
    return [v if v is not None else "" for v in str_list]
    
def get_ebit_run_info(odb_json):
    """
    Get the current scan configuration from the provided ODB.
    
    Args:
        
    * odb_json (dict) - ODB dump
    
    Returns:
        `EBITRunInfo`
    """
    run_info = EBITRunInfo()
    run_info.run_number = odb_json["Runinfo"]["Run number"]
    run_info.run_start_time = make_int(odb_json["Runinfo"]["Start time binary"])
    run_info.ppg_program = odb_json["Equipment"]["PPGCompiler"]["Programming"]
    
    x_enabled = odb_json["Scanning"]["Global"]["Settings"]["Enable X"]
    y_enabled = odb_json["Scanning"]["Global"]["Settings"]["Enable Y"]

    try:
        z_enabled = odb_json["Scanning"]["Global"]["Settings"]["Enable Z"]
    except KeyError:
        z_enabled = False

    run_info.num_x_steps = odb_json["Scanning"]["Global"]["Settings"]["nX steps"] if x_enabled else 0
    run_info.num_y_steps = odb_json["Scanning"]["Global"]["Settings"]["nY steps"] if y_enabled else 0
    run_info.num_z_steps = odb_json["Scanning"]["Global"]["Settings"]["nZ steps"] if z_enabled else 0
    
    epics_settings = odb_json["Scanning"]["Epics"]["Settings"]
    epics_dev_list = odb_json["Scanning"]["Epics"]["Definitions"]["Demand device"]
    
    if x_enabled:
        run_info.scan_ppg_x = make_none_empty_string(odb_json["Scanning"]["PPG"]["Settings"]["X paths"])
        run_info.scan_epics_x = [epics_dev_list[idx] if idx >= 0 else "" for idx in epics_settings["X devices"] ]
    
    if y_enabled:
        run_info.scan_ppg_y = make_none_empty_string(odb_json["Scanning"]["PPG"]["Settings"]["Y paths"])
        run_info.scan_epics_y = [epics_dev_list[idx] if idx >= 0 else "" for idx in epics_settings["Y devices"] ]
    
    if z_enabled and "Z paths" in odb_json["Scanning"]["PPG"]["Settings"]:
        run_info.scan_ppg_z = make_none_empty_string(odb_json["Scanning"]["PPG"]["Settings"]["Z paths"])
        run_info.scan_epics_z = [epics_dev_list[idx] if idx >= 0 else "" for idx in epics_settings["Z devices"] ]
        
    run_info.flip_bank_order = odb_json["Scanning"]["Global"]["Computed"]["FLIP bank order"]
    
    if not isinstance(run_info.flip_bank_order, list):
        run_info.flip_bank_order = [run_info.flip_bank_order]
        
    if len(run_info.flip_bank_order) == 1 and run_info.flip_bank_order[0] == "N/A":
        run_info.flip_bank_order = []
        
    for flip_device in run_info.flip_bank_order:
        try:
            true_meaning = odb_json["Scanning"][flip_device]["Computed"]["True meaning"]
        except KeyError:
            true_meaning = "True"
        try:
            false_meaning = odb_json["Scanning"][flip_device]["Computed"]["False meaning"]
        except KeyError:
            false_meaning = "False"
        
        run_info.flip_state_meanings[flip_device] = {True: true_meaning, False: false_meaning}
        
    run_info.initial_fc0inout_state = odb_json["Scanning"]["FC0InOut"]["Status"]["Set state"]
        
    if odb_json["Scanning"]["FaradayCupReadings"]["Settings"]["Enable logging"]:
        run_info.faraday_cup_pvs = odb_json["Scanning"]["FaradayCupReadings"]["Computed"]["PV list"]
    else:
        run_info.faraday_cup_pvs = []
    
    mca4_units = make_int(odb_json["Equipment"]["MCA4"]["Settings"]["Bin width units"]) # (ns, us, ms, s) for (0, 1, 2, 3)
    mca4_width = odb_json["Equipment"]["MCA4"]["Settings"]["Time bin width"]
     
    if mca4_units == 0:
        run_info.mca4_bin_width_ns = mca4_width
    elif mca4_units == 1:
        run_info.mca4_bin_width_ns = mca4_width * 1e3
    elif mca4_units == 2:
        run_info.mca4_bin_width_ns = mca4_width * 1e6
    elif mca4_units == 3:
        run_info.mca4_bin_width_ns = mca4_width * 1e9
        
    # Edit-on-start items
    eos = odb_json["Experiment"].get("Edit on Start", {})
    run_info.ion = eos.get("Ion", "")
    run_info.mcp = eos.get("MCP#", "")
    run_info.electron_current_mA = eos.get("Electron current (mA)", 0)
        
    return run_info

def get_ebit_event_info(midas_event, run_info):
    """
    Get scan setup for this event.
    
    Args:
        
    * midas_event (`midas.event.MidasEvent`)
    * run_info (`EBITRunInfo`)
    
    Returns:
        EBITEvent
    """
    ebit_event = EBITEvent()
    
    if not midas_event.bank_exists("SCAN"):
        return None
    
    ebit_event.original_midas_event = midas_event
    ebit_event.event_time = midas_event.header.timestamp
    
    # Scan metadata
    ebit_event.loop_count = midas_event.banks["SCAN"].data[0]
    ebit_event.x_step = midas_event.banks["SCAN"].data[1]
    ebit_event.y_step = midas_event.banks["SCAN"].data[2]

    ebit_event.z_step = -1 if len(midas_event.banks["SCAN"].data) < 7 else midas_event.banks["SCAN"].data[6]
       
    if midas_event.bank_exists("XPPG"):
        for i, path in enumerate(run_info.scan_ppg_x):
            if path != "" and path is not None:
                ebit_event.ppg_scan_values[path] = midas_event.banks["XPPG"].data[i]
                
    if midas_event.bank_exists("YPPG"):
        for i, path in enumerate(run_info.scan_ppg_y):
            if path != "" and path is not None:
                ebit_event.ppg_scan_values[path] = midas_event.banks["YPPG"].data[i]
                
    if midas_event.bank_exists("ZPPG"):
        for i, path in enumerate(run_info.scan_ppg_z):
            if path != "" and path is not None:
                ebit_event.ppg_scan_values[path] = midas_event.banks["ZPPG"].data[i]
        
    if midas_event.bank_exists("XEPD"):
        for i, dev in enumerate(run_info.scan_epics_x):
            if dev != "" and dev is not None:
                ebit_event.epics_scan_values[dev] = midas_event.banks["XEPD"].data[i]
                
    if midas_event.bank_exists("YEPD"):
        for i, dev in enumerate(run_info.scan_epics_y):
            if dev != "" and dev is not None:
                ebit_event.epics_scan_values[dev] = midas_event.banks["YEPD"].data[i]
                
    if midas_event.bank_exists("ZEPD"):
        for i, dev in enumerate(run_info.scan_epics_z):
            if dev != "" and dev is not None:
                ebit_event.epics_scan_values[dev] = midas_event.banks["ZEPD"].data[i]
    
    flip_states = {}
    
    if midas_event.bank_exists("FLIP"):
        if len(midas_event.banks["FLIP"].data) != len(run_info.flip_bank_order):
            raise ValueError("Unexpected number of flippable devices: %d/%d" % (len(midas_event.banks["FLIP"].data), len(run_info.flip_bank_order)))
        
        for i, state in enumerate(midas_event.banks["FLIP"].data):
            dev = run_info.flip_bank_order[i]
            flip_states[dev] = (state, run_info.flip_state_meanings[dev][state])
    
    ebit_event.fc0_inout = flip_states["FC0InOut"][1] if "FC0InOut" in flip_states else run_info.initial_fc0inout_state
    
    if midas_event.bank_exists("FCUP") and len(run_info.faraday_cup_pvs):
        if len(run_info.faraday_cup_pvs) != len(midas_event.banks["FCUP"].data):
            raise ValueError("Unexpected number of Faraday Cup readings: %d/%d" % (len(run_info.faraday_cup_pvs), len(midas_event.banks["FCUP"].data)))
        
        for i, reading in enumerate(midas_event.banks["FCUP"].data):
            ebit_event.faraday_cup_readings[run_info.faraday_cup_pvs[i]] = reading

    if midas_event.bank_exists("MCS1"):
        ebit_event.mca4_chan1 = midas_event.banks["MCS1"].data
        
    if midas_event.bank_exists("MCS2"):
        ebit_event.mca4_chan2 = midas_event.banks["MCS2"].data
                
    return ebit_event
