import numpy as np
import math
import statistics
import datetime

class PlotlyJson:
    """
    Helper functions for converting data into JSON that can be passed 
    to the javascript version of plotly.

    Members:

    * default_plot_height (int) - in pixels
    * default_plot_width (int) - in pixels
    * default_plot_bg_color (str) - plot background color
    """
    def __init__(self):
        self.default_plot_height = 600
        self.default_plot_width = 800
        self.default_plot_bg_color = "rgba(0,0,0,0)"

    def to_histo(self, data_arr, bins, range):
        """
        Convert a set to data points into a 1D histogram.
        
        Args:
            
        * data_arr (list of number)
        * bins (number) - number of bins to use. None => automatic
        * range ([number, number]) - axis range. None => automatic
        
        Returns:
            
        dict of settings for Plotly data
        """
        kwargs = {}
        
        if bins is not None:
            kwargs["bins"] = bins
        
        if range is not None:
            kwargs["range"] = range
        
        (y, edges) = np.histogram(data_arr, **kwargs)
        y = y.tolist()
        y.append(y[-1])
        
        return {"x": edges.tolist(),
                "y": y,
                "type": "scatter",
                "mode": "lines",
                "line": {"shape": "hv"}}

    def to_timestamp_plot(self, event_times, y_vals, name, needs_sorting=True, average_per_minute=False):
        """
        Create a plotly scatter plot, converting event times from `datetime.datetime`
        to strings in the format plotly can parse as dates.

        Args:

        * event_times (list of `datetime.datetime`)
        * y_vals (list of number) - one value per entry in `event_times`
        * name (string) - name of this plot for appearing in a legend
        * needs_sorting (bool)
        * average_per_minute (bool)
            
        Returns:
            
        dict of settings for Plotly data
        """
        if average_per_minute or needs_sorting:
            pairs = [(event_times[i], y_vals[i]) for i in range(min(len(event_times), len(y_vals)))]
            pairs = sorted(pairs, key=lambda x: x[0])
            sorted_event_times = [p[0] for p in pairs]
            sorted_y_vals = [p[1] for p in pairs]
        else:
            sorted_event_times = event_times    
            sorted_y_vals = y_vals

        if average_per_minute and len(sorted_event_times):
            start = sorted_event_times[0]
            gap = 60
            next = start + gap
            sum = 0
            count = 0
            show_x = []
            show_y = []

            for i in range(len(sorted_event_times)):
                if sorted_event_times[i] > next or i == len(sorted_event_times) - 1:
                    if count > 0:
                        show_x.append(next)
                        show_y.append(sum / count)

                    next += gap
                    sum = 0
                    count = 0

                if sorted_event_times[i] < next:
                    sum += sorted_y_vals[i]
                    count += 1
        else:
            show_x = sorted_event_times
            show_y = sorted_y_vals

        event_datetimes = [datetime.datetime.fromtimestamp(t) for t in show_x]
        
        if len(event_datetimes) and isinstance(show_x[0], int):
            # 1s precision
            fmt_str = "%Y-%m-%d %H:%M:%S"
            x_vals = [dt.strftime(fmt_str) for dt in event_datetimes]
        else:
            # 1ms precision
            fmt_str = "%Y-%m-%d %H:%M:%S.%f"
            x_vals = [dt.strftime(fmt_str)[:-3] for dt in event_datetimes]

        return {"x": x_vals,
                "y": show_y,
                "name": name,
                "type": "scattergl",
                "mode": "lines+markers"
               }

    def as_1d_plot(self, x_vals, y_vals, name, error_x=None, error_y=None):
        """
        Create a plotly scatter plot, with optional error bars.

        Args:

        * x_vals (list of number)
        * y_vals (list of number) - one value per entry in `x_vals`
        * name (string) - name of this plot for appearing in a legend
        * error_x (None, or list of number) - if None, no horizontal error bars will be drawn
        * error_y (None, or list of number) - if None, no vertical error bars will be drawn
            
        Returns:
            
        dict of settings for Plotly data
        """
        retval = {  "x": x_vals,
                    "y": y_vals,
                    "name": name,
                    "type": "scattergl",
                    "mode": "lines+markers",
                    "line": {"width": 0.5}
                  }

        if error_x is not None:
            retval["error_x"] = {
                   "type": 'data',
                   "array": error_x,
                   "visible": True
                }

        if error_y is not None:
            retval["error_y"] = {
                   "type": 'data',
                   "array": error_y,
                   "visible": True
                }

        return retval

    def to_heatmap(self, data_x, data_y, bins=None, range=None, name=None):
        """
        Convert a set to X,Y pairs into a 2D histogram (a heatmap in Plotly parlance).
        
        This function does the binning of the data for you! If you have pre-binned data,
        use `as_heatmap()` instead.
        
        Args:
            
        * data_x (list of number)
        * data_y (list of number) - one value per entry in `data_x` 
        * bins ([number, number]) - number of X,Y bins to use. None => automatic
        * range ([[number, number], [number, number]]) - X,Y range. None => automatic
        * name (string) - name of this plot for appearing in a legend
        
        Returns:
            
        dict of settings for Plotly data
        """
        (histo, x_edges, y_edges) = np.histogram2d(data_x, data_y, bins=bins, range=range)
        
        # Convert to plotly histogram (aka heatmap)
        # > numpy gives all bin edges, plotly wants bin centers
        # > numpy and plotly have opposite nesting of x/y lists
        return self.as_heatmap((x_edges[:-1]).tolist(), (y_edges[:-1]).tolist(), np.transpose(histo).tolist(), name)

    def as_heatmap(self, x_bins, y_bins, z, name):
        """        
        Create a plotly heatmap (2D histogram).

        Args:

        * x_bins (list of number) - center of each X bin
        * y_bins (list of number) - center of each Y bin
        * z (list of list of number) - data, outer index is Y bin, inner index is X bin
        * name (string) - name of this plot for appearing in a legend
            
        Returns:
            
        dict of settings for Plotly data
        """
        return {"x": x_bins, 
                "y": y_bins, 
                "z": z,
                "name": name,
                "type": "heatmap",
                "colorbar": {"thickness": 10},
                "colorscale": "Viridis",
                "zsmooth": False}

    def nan_to_zero(self, val):
        """
        Return the value, or 0 if value is NaN.
        """
        return 0 if math.isnan(val) else val

    def to_mean_rms_plot(self, data_dict, x_vals_list, include_rms=True, include_rms_as_2nd_plot=False, include_stderr=False):
        """
        Convert a set of data arrays into a plot of their means and stddevs.
        X axis values are specified by caller, Y axis values are computed.
        
        Args:
            
        * data_dict (dict of {int: list of numbers}) - the data to calculate
            the means/RMS of. Key is the index to use to look up the X axis
            value in `x_vals_list`.
        * x_vals_list (list of number) - Actual values for the X axis.
            If there's no corresponding entry in `data_dict`, the mean
            is set as None (so it is included on the X axis, but without
            a data point).
        * include_rms (bool) - Add error bars using standard deviation.
        * include_rms_as_2nd_plot (bool) - Return 2 plots, one with the data, one with the
            standard deviation of each point.
        * include_stderr (bool) - Add error bars using `std.dev./sqrt(n)` (aka standard error).
            
        Returns:
            
        If include_rms_as_2nd_plot is False:
            dict of settings for Plotly data
        If include_rms_as_2nd_plot is True:
            list of 2 dicts of settings for Plotly data
        """
        means = []
        rms = []
        counts = []
        
        for i in range(len(x_vals_list)):
            if i not in data_dict or data_dict[i] is None or len(data_dict[i]) == 0:
                means.append(None)
                rms.append(0)
                counts.append(0)
            else:
                means.append(self.nan_to_zero(statistics.mean(data_dict[i])))
                counts.append(len(data_dict[i]))
                
                if len(data_dict[i]) == 1:
                    rms.append(0)
                else:
                    rms.append(self.nan_to_zero(statistics.stdev(data_dict[i])))

        stderr = []

        for i in range(len(rms)):
            stderr.append(0 if counts[i] == 0 else rms[i]/math.sqrt(counts[i]))

        if include_rms:
            means_plot = self.as_1d_plot(x_vals_list, means, "", error_y=rms)
        elif include_stderr:
            means_plot = self.as_1d_plot(x_vals_list, means, "", error_y=stderr)
        else:
            means_plot = self.as_1d_plot(x_vals_list, means, "")

        if include_rms_as_2nd_plot:
            return [means_plot, self.as_1d_plot(x_vals_list, rms, "")]
        else:
            return means_plot
        
    def get_default_layout(self, x_title="X", y_title="Y", legend=False):
        """
        Get the default layout we should use for a plot.
        
        Args:
            
        * x_title (string) - title for the X axis
        * y_title (string) - title for the Y axis
        * legend (boolean) - whether to show a legend
        
        Returns:
            
        dict of settings for Plotly layout
        """
        return {"showlegend": legend,
                "uirevision": "1", # change value to force a zoom-out
                "hovermode": "closest",
                "height": self.default_plot_height,
                "width": self.default_plot_width,
                "margin": {
                  "l": 60,
                  "r": 10,
                  "b": 40,
                  "t": 10,
                  "pad": 4
                },
                "plot_bgcolor": self.default_plot_bg_color,
                "paper_bgcolor": self.default_plot_bg_color,
                "yaxis": {
                  "title": y_title
                },
                "xaxis": {
                  "title": x_title
                },
              }

    def format_as_side_by_side_heatmaps(self, plots, layout, x_axis_int=False, y_axis_int=False):
        """
        If there are 2 plots, format them to appear side-by-side, with
        annotations that act like subplot titles.

        Updates the `layout` argument in-place!

        Args:

        * plots (list of dict) - dicts as returned by `as_heatmap()` or `to_heatmap()`
        * layout (dict) - dict as returned by `get_default_layout()`
        * x_axis_int (boolean) - whether to only show integer values on X axis
        * y_axis_int (boolean) - whether to only show integer values on X axis
        """
        if len(plots) > 1:
            layout["width"] *= 1.5
            layout["xaxis2"] = {"title": layout["xaxis"]["title"]}
            layout["yaxis2"] = {} # Share title with first plot
            layout["grid"] = {"rows": 1, "columns": 2}
            layout["margin"]["t"] = 60
            layout["annotations"] = [
                {"text": plots[0]["name"],
                 "font": {"size": 14},
                 "showarrow": False,
                 "align": "center",
                 "x": 0.13,
                 "y": 1.1,
                 "xref": "paper",
                 "yref": "paper"},
                {"text": plots[1]["name"],
                 "font": {"size": 14},
                 "showarrow": False,
                 "align": "center",
                 "x": 0.9,
                 "y": 1.1,
                 "xref": "paper",
                 "yref": "paper"}]

        if x_axis_int:
            layout["xaxis"]["tickformat"] = ",d"

            if len(plots) > 1:
                layout["xaxis2"]["tickformat"] = ",d"
                
        if y_axis_int:
            layout["yaxis"]["tickformat"] = ",d"

            if len(plots) > 1:
                layout["yaxis2"]["tickformat"] = ",d"
