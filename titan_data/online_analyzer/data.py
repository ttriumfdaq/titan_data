"""
Helper classes for keeping per-event summary info and per-run scan config in memory.
"""

class EventBase:
    """
    Base class for experimental-specific summaries of each event.

    Members:

    * event_time (datetime)
    * loop_count (int)
    """
    def __init__(self):
        self.event_time = None
        self.loop_count = None

class RunScanVars:
    """
    Helper tools for tracking the scanned variables this run.
    Includes functions for getting human-readable explanations of the scan config.

    The concept is that each time you see a new event, you call `update()` on this
    object, and we'll store the scanned variables in a map vs the current X/Y step.

    Members:

    * nx (int) - number of X scanned values this run (number of X steps + 1)
    * ny (int) - number of Y scanned values this run (number of Y steps + 1)
    * nz (int) - number of Z scanned values this run (number of Z steps + 1)
    * run_scan_vars_x (dict of {str: [number]}) - scanned variable name to list of values (per step)
    * run_scan_vars_y (dict of {str: [number]}) - scanned variable name to list of values (per step)
    * run_scan_vars_z (dict of {str: [number]}) - scanned variable name to list of values (per step)
    """
    def __init__(self):
        self.nx = 0
        self.ny = 0
        self.nz = 0
        self.run_scan_vars_x = {}
        self.run_scan_vars_y = {}
        self.run_scan_vars_z = {}

    def configure(self, nx, ny, nz, scan_var_names_x, scan_var_names_y, scan_var_names_z):
        """
        Call this at begin-of-run to setup the number of X/Y steps, and the names of the
        variables being scanned this run.

        Args:

        * nx (int) - number of X scanned values this run (number of X steps + 1)
        * ny (int) - number of Y scanned values this run (number of Y steps + 1)
        * nz (int) - number of Z scanned values this run (number of Z steps + 1)
        * scan_var_names_x (list of str)
        * scan_var_names_y (list of str)
        * scan_var_names_z (list of str)
        """
        self.nx = nx
        self.ny = ny
        self.nz = nz
        self.run_scan_vars_x = {i: {x: None for x in scan_var_names_x} for i in range(nx)}
        self.run_scan_vars_y = {i: {y: None for y in scan_var_names_y} for i in range(ny)}
        self.run_scan_vars_z = {i: {z: None for z in scan_var_names_z} for i in range(nz)}

    def update(self, x_step, y_step, z_step, scan_vals):
        """
        Call this each time an event is seen.

        Args:

        * x_step (int)
        * y_step (int)
        * z_step (int)
        * scan_vals (dict of {str: number}) - keys should match the names previously passed to `configure()`.
        """
        for k, v in scan_vals.items():
            if k in self.run_scan_vars_x[x_step]:
                self.run_scan_vars_x[x_step][k] = v
                
            if k in self.run_scan_vars_y[y_step]:
                self.run_scan_vars_y[y_step][k] = v
                
            if k in self.run_scan_vars_z[z_step]:
                self.run_scan_vars_z[z_step][k] = v

    def format_scan_value(self, val):
        """
        Format a number in a pretty representation - as a float,
        but with trailing zeroes truncated.
        """
        try:
            return ("%.6f" % val).rstrip("0").rstrip(".")
        except:
            return str(val)

    def get_x_scan_label(self, step=None):
        """
        Get text for a histogram axis or trace label.

        If a single value is being scanned, use the name of the variable; if more
        than one value is being scanned, use "X step".

        Args:
        
        * step (int or None) - if not None, include the value of the variable at that step

        Returns:
            str - e.g. "MY:EPICS:PV", "MY:EPICS:PV = 1.23", "X step = 2"
        """
        retval = self.x_first_var_name() if self.is_x_single_var() else "X step"

        retval = retval.replace("/Equipment/PPGCompiler/Programming/", "")

        if step is not None:
            retval += " = " + self.format_scan_value(self.get_x_scan_list()[step])

        return retval

    def get_y_scan_label(self, step=None):
        """
        Get text for a histogram axis or trace label.

        If a single value is being scanned, use the name of the variable; if more
        than one value is being scanned, use "Y step".

        Args:
        
        * step (int or None) - if not None, include the value of the variable at that step

        Returns:
            str - e.g. "MY:EPICS:PV", "MY:EPICS:PV = 1.23", "Y step = 2"
        """
        retval = self.y_first_var_name() if self.is_y_single_var() else "Y step"

        if step is not None:
            retval += " = " + self.format_scan_value(self.get_y_scan_list()[step])

        return retval

    def get_z_scan_label(self, step=None):
        """
        Get text for a histogram axis or trace label.

        If a single value is being scanned, use the name of the variable; if more
        than one value is being scanned, use "Z step".

        Args:
        
        * step (int or None) - if not None, include the value of the variable at that step

        Returns:
            str - e.g. "MY:EPICS:PV", "MY:EPICS:PV = 1.23", "Z step = 2"
        """
        retval = self.z_first_var_name() if self.is_z_single_var() else "Z step"

        if step is not None:
            retval += " = " + self.format_scan_value(self.get_z_scan_list()[step])

        return retval

    def get_x_scan_list(self):
        """
        Get the list of values being scanned in X direction.

        If a single value is being scanned, use the actual values; if more than
        one value is being scanned, use the step numbers.

        Returns:
            list of number
        """
        if self.is_x_single_var():
            first_var = self.x_first_var_name()
            retval = [None] * self.nx

            for step, state in self.run_scan_vars_x.items():
                retval[step] = state[first_var]
            
            return retval
        else:
            return list(range(self.nx))

    def get_y_scan_list(self):
        """
        Get the list of values being scanned in Y direction.

        If a single value is being scanned, use the actual values; if more than
        one value is being scanned, use the step numbers.

        Returns:
            list of number
        """
        if self.is_y_single_var():
            first_var = self.y_first_var_name()
            retval = [None] * self.ny

            for step, state in self.run_scan_vars_y.items():
                retval[step] = state[first_var]

            return retval
        else:
            return list(range(self.ny))

    def get_z_scan_list(self):
        """
        Get the list of values being scanned in Z direction.

        If a single value is being scanned, use the actual values; if more than
        one value is being scanned, use the step numbers.

        Returns:
            list of number
        """
        if self.is_z_single_var():
            first_var = self.z_first_var_name()
            retval = [None] * self.nz

            for step, state in self.run_scan_vars_z.items():
                retval[step] = state[first_var]

            return retval
        else:
            return list(range(self.nz))

    def is_x_scanned(self):
        """
        Whether X scanning is active for this run.

        Returns:
            boolean
        """
        return self.nx > 1

    def is_y_scanned(self):
        """
        Whether Y scanning is active for this run.

        Returns:
            boolean
        """
        return self.ny > 1

    def is_z_scanned(self):
        """
        Whether Z scanning is active for this run.

        Returns:
            boolean
        """
        return self.nz > 1

    def is_x_single_var(self):
        """
        Whether X scanning is active for this run and only a single variable
        is being scanned.

        Returns:
            boolean
        """
        return self.is_x_scanned() and len(self.run_scan_vars_x[0]) == 1

    def is_y_single_var(self):
        """
        Whether Y scanning is active for this run and only a single variable
        is being scanned.

        Returns:
            boolean
        """
        return self.is_y_scanned() and len(self.run_scan_vars_y[0]) == 1

    def is_z_single_var(self):
        """
        Whether Z scanning is active for this run and only a single variable
        is being scanned.

        Returns:
            boolean
        """
        return self.is_z_scanned() and len(self.run_scan_vars_z[0]) == 1

    def x_first_var_name(self):
        """
        Get the name of a variable being scanned in the X direction.

        Returns:
            string
        """
        var_list = list(self.run_scan_vars_x[0].keys())

        if len(var_list) > 0:
            return var_list[0]
        else:
            return None

    def y_first_var_name(self):
        """
        Get the name of a variable being scanned in the Y direction.

        Returns:
            string
        """
        var_list = list(self.run_scan_vars_y[0].keys())

        if len(var_list) > 0:
            return var_list[0]
        else:
            return None

    def z_first_var_name(self):
        """
        Get the name of a variable being scanned in the Z direction.

        Returns:
            string
        """
        var_list = list(self.run_scan_vars_z[0].keys())

        if len(var_list) > 0:
            return var_list[0]
        else:
            return None

    def x_readable(self):
        """
        Get a somewhat human-readable explanation of the X scan config this run.

        Returns:
            list of dict of {str: number or string}. List index is the step number,
            key is the variable name, value is the current value or the string
            "Not seen yet" if we don't know what that value is yet.
        """
        retval = [None] * self.nx

        for step, state in self.run_scan_vars_x.items():
            retval[step] = {}

            for k, v in state.items():
                retval[step][k] = v if v is not None else "Not seen yet"

        return retval

    def y_readable(self):
        """
        Get a somewhat human-readable explanation of the Y scan config this run.

        Returns:
            list of dict of {str: number or string}. List index is the step number,
            key is the variable name, value is the current value or the string
            "Not seen yet" if we don't know what that value is yet.
        """
        retval = [None] * self.ny
        
        for step, state in self.run_scan_vars_y.items():
            retval[step] = {}

            for k, v in state.items():
                retval[step][k] = v if v is not None else "Not seen yet"

        return retval

    def z_readable(self):
        """
        Get a somewhat human-readable explanation of the Z scan config this run.

        Returns:
            list of dict of {str: number or string}. List index is the step number,
            key is the variable name, value is the current value or the string
            "Not seen yet" if we don't know what that value is yet.
        """
        retval = [None] * self.ny
        
        for step, state in self.run_scan_vars_z.items():
            retval[step] = {}

            for k, v in state.items():
                retval[step][k] = v if v is not None else "Not seen yet"

        return retval

def filter_timespan(ev_list, timespan):
    """
    Take a list of `EventBase`, and return a list of only
    the events that pass our timespan cut.
    
    Args:
        
    * ev_list (list of `EventBase`) - these should be pre-sorted to be 
        in time-ascending order!
    * timespan ({"type": string, "n": number}) - timespan cut. Allowed
        values for "type" are "run", "n_loops" and "n_minutes". "n" is
        the number of loops/minutes.
    """
    evs_in_timespan = []
    first_loop = None
    first_time = None
    
    if timespan["n"] is None:
        timespan["n"] = 10
    
    for ev in reversed(ev_list):
        if first_loop is None:
            first_loop = ev.loop_count
            first_time = ev.event_time

        if timespan["type"] == "n_loops" and ev.loop_count <= first_loop - timespan["n"]:
            break
        
        if timespan["type"] == "n_minutes" and ev.event_time <= first_time - (timespan["n"] * 60):
            break
        
        evs_in_timespan.append(ev)

    evs_in_timespan.reverse()
    return evs_in_timespan

def passes_cut(val, limits=[None, None]):
    """
    Whether a value passes a cut.
    
    Args:
        
    * val (number) - the value to test
    * limits ([number, number]) - the min/max values to test against.
        None means no min/max cut.
    
    Returns:
        
    boolean
    """
    if limits[0] is not None and val < limits[0]:
        return False
    if limits[1] is not None and val > limits[1]:
        return False
    return True