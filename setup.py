from setuptools import setup

setup(name='titan_data',
      version='0.1',
      description='Python tools for reading TITAN PPG midas files',
      author='Ben Smith',
      author_email='bsmith@triumf.ca',
      packages=['titan_data'])